//
//  ListOfTicketsViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 22/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import <RestKit/RestKit.h>


@interface ListOfTicketsViewController : UITableViewController<RKObjectLoaderDelegate, RKRequestDelegate>{
    RKObjectManager *objectManager;
    NSInteger numberOfTickets;
}

@property (nonatomic, retain)NSMutableArray *conteudos;
@property (nonatomic, retain)NSMutableArray *ticketsPurchased;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
