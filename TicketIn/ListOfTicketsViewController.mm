//
//  TicketsViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 21/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "ListOfTicketsViewController.h"
#import "TicketCell.h"
#import "Ingresso.h"
#import "TiposDeIngressos.h"
#import "QREncoder.h"
#import "DataMatrix.h"
#import "QRCodeViewController.h"
#import "LocalizationViewController.h"
#import "HUD.h"

@interface ListOfTicketsViewController ()

@end

@implementation ListOfTicketsViewController

@synthesize tableView = _tableView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [HUD showUIBlockingIndicatorWithText:@"Carregando ingressos..."];
    
    [self.tableView setFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - 61)];
    UIImage *originalImage = [UIImage imageNamed:@"bg.jpg"];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[self imageWithImage:originalImage convertToSize:self.view.bounds.size]];
    [self.tableView setBackgroundView:backgroundView];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    objectManager = [RKObjectManager sharedManager];
    [objectManager loadObjectsAtResourcePath:@"/ingressos" delegate:self];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor colorWithRed:0 green:0.3 blue:0 alpha:1.0];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    numberOfTickets = 0;
}

-(void)refreshTable{
    [objectManager loadObjectsAtResourcePath:@"/ingressos" delegate:self];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return numberOfTickets;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"TicketCell";
    
    //tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divisor.png"]];
    
    TicketCell *cell = (TicketCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TicketCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        UIImage *separatorImage = [UIImage imageNamed:@"divisor.png"];
        UIImageView *separatorImageView = [[UIImageView alloc] initWithImage:separatorImage];
        [separatorImageView setFrame:CGRectMake(0, cell.frame.size.height, separatorImage.size.width, separatorImage.size.height)];
        [cell addSubview:separatorImageView];
    }
    
    // Configure the cell...
    
    Conteudo *conteudo;
    for (Conteudo *cont in self.conteudos) {
        if ([cont.conteudoId isEqualToNumber:[[self.ticketsPurchased objectAtIndex:indexPath.row] conteudoId]])
            conteudo = cont;
    }
    TiposDeIngressos *tDI;
    for (TiposDeIngressos *t in conteudo.tiposDeIngressos) {
        if ([t.tiposDeIngressosId isEqualToNumber:[[self.ticketsPurchased objectAtIndex:indexPath.row] tipoDeIngressoId]]) {
            tDI = t;
        }
    }
    
    cell.conteudo = conteudo;
    cell.showName.text = conteudo.titulo;
    cell.showType.text = tDI.titulo;
    
    NSCalendar *calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDate *date = conteudo.data;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date];
    
    NSInteger year = [dateComponents year];
    NSInteger month = [dateComponents month];
    NSInteger day = [dateComponents day];
    NSInteger hour = [dateComponents hour];
    NSInteger min = [dateComponents minute];
    
    NSString *showTime = [NSString stringWithFormat:@"%02d:%02d", hour, min];
    NSString *showDate = [NSString stringWithFormat:@"%02d.%02d.%d", day, month, year];
    cell.showDate.text = showDate;
    cell.showHour.text = showTime;
    
    //the qrcode is square. now we make it 250 pixels wide
   
    if ([[self.ticketsPurchased objectAtIndex:indexPath.row] resposta]) {
        int qrcodeImageDimension = 120;
        //the string can be very long
        cell.QRCode = [[self.ticketsPurchased objectAtIndex:indexPath.row] resposta];
        
        //first encode the string into a matrix of bools, TRUE for black dot and FALSE for white. Let the encoder decide the error correction level and version
        DataMatrix* qrMatrix = [QREncoder encodeWithECLevel:QR_ECLEVEL_AUTO version:QR_VERSION_AUTO string:cell.QRCode];
        
        //then render the matrix
        UIImage* qrcodeImage = [QREncoder renderDataMatrix:qrMatrix imageDimension:qrcodeImageDimension];
        
        //put the image into the view
        
        //and that's it!
        [cell.imageView setImage:qrcodeImage];
    }
    else{
        [cell.imageView setImage:[UIImage imageNamed:@"processing.jpg"]];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [button setTag:indexPath.row];
    CGRect buttonFrame = button.frame;
    [button setFrame:CGRectMake(cell.frame.size.width - buttonFrame.size.width - 5, cell.frame.size.height - buttonFrame.size.height - 5, buttonFrame.size.width, buttonFrame.size.height)];
    [cell addSubview:button];
    [button addTarget:self action:@selector (ActionMethod:)
       forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TicketCell *cell = (TicketCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([[self.ticketsPurchased objectAtIndex:indexPath.row] resposta]){
        QRCodeViewController *QRCVC = [[QRCodeViewController alloc] initWithStringAndConteudo:cell.QRCode conteudo:cell.conteudo];
        [self.navigationController pushViewController:QRCVC animated:YES];
    }
    else{
        QRCodeViewController *QRCVC = [[QRCodeViewController alloc] initWithStringAndConteudo:@"" conteudo:cell.conteudo];
        [self.navigationController pushViewController:QRCVC animated:YES];
    }
}

#pragma mark private methods

-(void) ActionMethod:(UIButton *)button
{
    NSIndexPath *indexPAth = [NSIndexPath indexPathForItem:button.tag inSection:0];
    TicketCell *cell = (TicketCell *)[self.tableView cellForRowAtIndexPath:indexPAth];
    
    LocalizationViewController *lVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalizationView"];
    lVC.conteudo = cell.conteudo;
    [self.navigationController pushViewController:lVC animated:YES];
    
}

-(void)loadObjectsFromDataStore{
    NSFetchRequest *request = [Ingresso fetchRequest];
    NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    
    self.ticketsPurchased = [NSMutableArray arrayWithArray:[Ingresso objectsWithFetchRequest:request]];
    for (Ingresso *i in [self.ticketsPurchased copy]) {
        if (!i.transacao) {
            [self.ticketsPurchased removeObject:i];
        }
    }
    
    numberOfTickets = self.ticketsPurchased.count;
    
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark ObjectLoaderDelegate

-(void)objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastUpdatedAt"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    [self loadObjectsFromDataStore];
    [HUD hideUIBlockingIndicator];
}

-(void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error{
    [self loadObjectsFromDataStore];
    [HUD hideUIBlockingIndicator];
}

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
            NSLog(@"Retrieved XML: %@", [response bodyAsString]);
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            
        }
        
    } else if ([request isDELETE]) {
        
        if ([response isNotFound]) {
            NSLog(@"Not Found");
        }
        else{
            [objectManager loadObjectsAtResourcePath:@"/ingressos" delegate:self];
        }
    }

}

@end
