//
//  CadastreRowInfo.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CadastreRowInfo : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *textOfTextfield;
@property (nonatomic, retain) NSNumber *validValue;

+(NSMutableArray *)listOfItens;

@end