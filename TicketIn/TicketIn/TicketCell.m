//
//  TicketCell.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 24/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "TicketCell.h"

@implementation TicketCell

@synthesize QRCode = _QRCode;
@synthesize conteudo = _conteudo;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
