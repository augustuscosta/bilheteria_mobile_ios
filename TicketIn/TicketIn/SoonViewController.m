//
//  SoonViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "SoonViewController.h"
#import "EventDetailViewController.h"
#import "AsyncImageView.h"

@interface SoonViewController ()

@end

@implementation SoonViewController

@synthesize delegate = _delegate;
@synthesize allTableData = _allTableData;
@synthesize filteredTableData = _filteredTableData;

@synthesize soonConteudos = _soonConteudos;
@synthesize searchBar;
@synthesize isFiltered = _isFiltered;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchBar.delegate = (id)self;
    searchBar.backgroundColor = [UIColor clearColor];
    for (UIView *subview in searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
    
    [self.tableView setFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - 61)];
    UIImage *originalImage = [UIImage imageNamed:@"bg.jpg"];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[self imageWithImage:originalImage convertToSize:self.view.bounds.size]];
    [self.tableView setBackgroundView:backgroundView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount;
    if(self.isFiltered)
        rowCount = self.filteredTableData.count;
    else
        rowCount = self.soonConteudos.count;
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil)
      //  cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    Conteudo* conteudo;
    if(self.isFiltered)
        conteudo = [self.filteredTableData objectAtIndex:indexPath.row];
    else
        conteudo = [self.soonConteudos objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = conteudo.titulo;
    //cell.detailTextLabel.text = conteudo.subtitulo;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"loading_ticket.png"];
    imageView.imageURL = [NSURL URLWithString:conteudo.rowImagemUrl];
    [cell setBackgroundView:imageView];
    
    return cell;
}

#pragma mark SearchBar Delegate methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1;
{
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBarr {
    [searchBarr resignFirstResponder];
    searchBar.showsCancelButton = NO;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBarr{
    searchBar.showsCancelButton = YES;
    return YES;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isFiltered = FALSE;
    }
    else
    {
        self.isFiltered = true;
        self.filteredTableData = [[NSMutableArray alloc] init];
        
        for (Conteudo* conteudo in self.soonConteudos)
        {
            NSRange nameRange = [conteudo.titulo rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [conteudo.subtitulo rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [self.filteredTableData addObject:conteudo];
            }
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Conteudo *conteudo;
    if(self.isFiltered)
        conteudo = [self.filteredTableData objectAtIndex:indexPath.row];
    else
        conteudo = [self.soonConteudos objectAtIndex:indexPath.row];
    
    if ([self.delegate respondsToSelector:@selector(handleSelectRowOnSoonEvents:)]) {
        [self.delegate handleSelectRowOnSoonEvents:conteudo];
    }
}

@end
