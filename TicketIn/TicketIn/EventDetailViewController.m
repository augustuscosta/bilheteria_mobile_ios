//
//  EventDetailViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventDetailViewController.h"
#import "CollectionViewCell.h"
#import "PresentImageViewController.h"
#import "TiposDeIngressos.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

@synthesize backgroundView = _backgroundView;
@synthesize conteudo = _conteudo;
@synthesize scrollView = _scrollView;
@synthesize collectionView = _collectionView;
@synthesize BuyButton = _BuyButton;
@synthesize webView = _webView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.title = self.conteudo.titulo;
    

    [self.backgroundView setImage:[self imageWithImage:[UIImage imageNamed:@"bg.jpg"] convertToSize:self.view.bounds.size]];
    
    [self createCollectionView];
    
    [self createBuyButton];
    
    self.webView = [[UIWebView alloc] init];
    self.webView.opaque = NO;
    self.webView.backgroundColor = [UIColor clearColor];
    
    self.webView.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSString *htmlString = self.conteudo.descricao;
    [self.webView loadHTMLString:htmlString baseURL:nil];
    
    EventImageView *topImage = [[EventImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 300) url:[NSURL URLWithString:self.conteudo.imagemUrl]];
    topImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.scrollView addSubview:topImage];
    
    [self.webView setFrame:CGRectMake(0, topImage.frame.size.height + 40, self.view.frame.size.width, 1)];
    float numberOfLines = MAX(1, ((int)(self.conteudo.childs.count/3)));
    float collectionViewHeight = ((numberOfLines * 110) -10);
    if (collectionViewHeight == 100)
        collectionViewHeight = 0;
    [self.collectionView setFrame:CGRectMake(0, self.webView.frame.origin.y + self.webView.frame.size.height + 40, self.view.bounds.size.width, collectionViewHeight)];
    
    CGRect buyButtonFrame = self.BuyButton.frame;
    [self.BuyButton setFrame:CGRectMake(self.view.bounds.size.width/2 - buyButtonFrame.size.width/2, self.collectionView.frame.origin.y + self.collectionView.frame.size.height + 40, buyButtonFrame.size.width, buyButtonFrame.size.height)];
    
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    [self.scrollView addSubview:self.webView];
    [self.scrollView addSubview:self.collectionView];
    
    NSDate *conteudoInicioVendasDate = self.conteudo.dataInicioVendas;
    NSDate *deviceCurrentDate = [NSDate date];
    NSComparisonResult comparisonInicioVendas = [deviceCurrentDate compare:conteudoInicioVendasDate];
    
    if (comparisonInicioVendas != NSOrderedAscending) {
        [self.scrollView addSubview:self.BuyButton];
    }
}


-(void)viewDidAppear:(BOOL)animated{

    //
    
}

#pragma mark private methods

-(void)handleBuyButton{
    [self performSegueWithIdentifier:@"BuySegue" sender:self];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView setBackgroundColor:[UIColor clearColor]];
    [self performSelector:@selector(calculateWebViewSize) withObject:nil afterDelay:0.1];
}

- (void) calculateWebViewSize {
    
    if ([self.webView scrollView].contentSize.height == 0) {
        [self performSelector:@selector(calculateWebViewSize) withObject:nil afterDelay:0.1];
        return;
    }
    CGRect wbFrame = self.webView.frame;
    [self.webView setFrame:CGRectMake(wbFrame.origin.x, wbFrame.origin.y, wbFrame.size.width, [self.webView scrollView].contentSize.height)];
    
    
    float numberOfLines = MAX(0, ceil((float)self.conteudo.childs.count/3));
    float collectionViewHeight = ((numberOfLines * 110) -10);
    if (collectionViewHeight < 100)
        collectionViewHeight = 0;
    [self.collectionView setFrame:CGRectMake(0, wbFrame.origin.y + [self.webView scrollView].contentSize.height + 20, self.view.bounds.size.width, collectionViewHeight)];
    
    CGRect buyButtonFrame = self.BuyButton.frame;
    [self.BuyButton setFrame:CGRectMake(self.view.bounds.size.width/2 - buyButtonFrame.size.width/2, self.collectionView.frame.origin.y + self.collectionView.frame.size.height + 40, buyButtonFrame.size.width, buyButtonFrame.size.height)];
    
    NSDate *conteudoInicioVendasDate = self.conteudo.dataInicioVendas;
    NSDate *deviceCurrentDate = [NSDate date];
    NSComparisonResult comparisonInicioVendas = [deviceCurrentDate compare:conteudoInicioVendasDate];
    
    if (comparisonInicioVendas != NSOrderedAscending) {
        self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, MAX(self.BuyButton.frame.size.height + self.BuyButton.frame.origin.y , self.view.bounds.size.height));
    }
    
    else{
        self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, MAX(self.collectionView.frame.origin.y + self.collectionView.frame.size.height, self.view.bounds.size.height));
    }
    
    [self.webView setHidden:NO];
    //}
    //do some stuff with the webView.frame.size.height
}

#pragma mark Segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"BuySegue"]) {
        purchaseOptionsTableViewController *pOTVC = segue.destinationViewController;
        pOTVC.conteudo = self.conteudo;
    }
}

#pragma mark create methods

-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
}

-(void)createBuyButton{
    self.BuyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.BuyButton setTitle:@"Comprar" forState:UIControlStateNormal];
    [self.BuyButton addTarget:self action:@selector(handleBuyButton) forControlEvents:UIControlEventTouchDown];
    UIImage *buyButtonImage = [UIImage imageNamed:@"selectTicket.png"];
    [self.BuyButton setImage:buyButtonImage forState:UIControlStateNormal];
    [self.BuyButton setFrame:CGRectMake(0, 0, buyButtonImage.size.width, buyButtonImage.size.height)];
    
}

#pragma mark Collection delegate

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.conteudo.childs.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    CollectionViewCell *cvc = (CollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    cvc.collectionViewImage.image = [UIImage imageNamed:@"loader.png"];
    cvc.collectionViewImage.imageURL =  [NSURL URLWithString:[[self.conteudo.childs objectAtIndex:indexPath.item] imagemUrl]];
    cvc.backgroundColor = [UIColor grayColor];
    
    return cvc;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PresentImageViewController *pIVC = [[PresentImageViewController alloc] init];
    pIVC.conteudo = [self.conteudo.childs objectAtIndex:indexPath.item];
    [self.navigationController pushViewController:pIVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

@end
