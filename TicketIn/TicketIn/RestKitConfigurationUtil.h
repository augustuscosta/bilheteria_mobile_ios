//
//  RestKitConfigurationUtil.h
//  skynet
//
//  Created by Augustus Costa on 3/1/12.
//  Copyright (c) 2012 AfterSix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface RestKitConfigurationUtil : NSObject

-(void)initRestKit;

@end
