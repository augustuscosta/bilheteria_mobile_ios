//
//  CheckoutCell.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 03/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "CheckoutCell.h"

@implementation CheckoutCell

@synthesize subtotalValue = _subtotalValue;
@synthesize chargeValue = _chargeValue;
@synthesize totalValue = _totalValue;
@synthesize paypalButton = _paypalButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIImage *backgroundImage = [UIImage imageNamed:@"barra.png"];
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
        [self.contentView addSubview:backgroundView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
