//
//  CollectionViewCell.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CollectionViewCell.h"


@implementation CollectionViewCell

@synthesize collectionViewImage = _collectionViewImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
