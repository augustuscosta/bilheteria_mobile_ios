//
//  ItemRegistrationCell.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemRegistrationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleName;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@end