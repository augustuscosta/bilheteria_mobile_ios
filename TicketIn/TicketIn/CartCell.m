//
//  CartCell.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 26/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CartCell.h"

@implementation CartCell

@synthesize ShowName = _ShowName;
@synthesize ShowDate = _ShowDate;
@synthesize ShowTime = _ShowTime;
@synthesize TicketType = _TicketType;
@synthesize TicketValue = _TicketValue;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
