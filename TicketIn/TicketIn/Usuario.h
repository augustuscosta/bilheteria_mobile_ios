//
//  Usuario.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 06/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Usuario : NSObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * adress;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * cep;
@property (nonatomic, retain) NSString * rg;
@property (nonatomic, retain) NSString * cpf;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSDate * birthdate;
@property (nonatomic, retain) NSString * password;

@end
