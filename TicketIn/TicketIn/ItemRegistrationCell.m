//
//  ItemRegistrationCell.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "ItemRegistrationCell.h"

@implementation ItemRegistrationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
