//
//  FeaturedViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 16/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "Conteudo.h"

@protocol FeaturedViewControllerDelegate <NSObject>


-(void)handleTapGestureOnFeaturedObject:(Conteudo *)conteudo;

@end

@interface FeaturedViewController : UIViewController<UIGestureRecognizerDelegate>{
    id <FeaturedViewControllerDelegate>delegate;
    NSTimer *autoTimer;
}

@property (nonatomic, retain) id <FeaturedViewControllerDelegate>delegate;
@property (nonatomic, retain) NSMutableArray *arrayWithFeaturedObjects;

@end
