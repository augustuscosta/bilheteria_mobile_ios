//
//  MapPoint.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 18/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "MapPoint.h"

@implementation MapPoint

@synthesize title = _title;
@synthesize subtitle = _subtitle;
@synthesize coordinate = _coordinate;
@synthesize conteudo = _conteudo;

-(id) initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t subtitle:(NSString *)sub conteudo:(Conteudo *)ct{
    self = [super init];
    if (self != nil) {
        _coordinate = c;
        _title = t;
        _subtitle = sub;
        _conteudo = ct;
    }
    return self;
}

@end
