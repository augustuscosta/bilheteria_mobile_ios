//
//  purchaseOptionsTableViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 17/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "purchaseOptionsTableViewController.h"
#import "Ingresso.h"
#import "TiposDeIngressos.h"
#import "Usuario.h"
#import "ContainerViewController.h"
#import "HUD.h"

@interface purchaseOptionsTableViewController ()

@end

@implementation purchaseOptionsTableViewController

@synthesize tiposDeIngressos = _tiposDeIngressos;
@synthesize conteudo = _conteudo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    objectManager = [RKObjectManager sharedManager];
    
    self.title = @"Detalhes do Ingresso";
    NSLog(@"%@", self.conteudo.titulo);
    self.tiposDeIngressos = [NSMutableArray arrayWithArray:[self.conteudo.tiposDeIngressos allObjects]];
    
    numberOfTicketsSelectedIndex = -1;
    typeOfTicketSelectedIndex = -1;
    searchNumberOfTyckets = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20" , nil];
    
    UIImage *originalImage = [UIImage imageNamed:@"bg.jpg"];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[self imageWithImage:originalImage convertToSize:self.view.bounds.size]];
    [self.tableView setBackgroundView:backgroundView];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 2;
    }
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    if (indexPath.section == 1) {
        for (UIView *view in cell.subviews) {
            [view removeFromSuperview];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImage *addToCartImage = [UIImage imageNamed:@"adicionarcarrinho.png"];
        UIImageView *cellImageView = [[UIImageView alloc] initWithImage:addToCartImage];
        CGRect cellImageViewFrame = CGRectMake(cell.frame.origin.x + cell.frame.size.width/2 - addToCartImage.size.width/2, cell.frame.size.height/2 - addToCartImage.size.height/2, cellImageView.frame.size.width, cellImageView.frame.size.height);
        [cellImageView setFrame:cellImageViewFrame];
        [cell addSubview:cellImageView];
        return cell;
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Tipo";
            if (self.tiposDeIngressos.count > 0) {
                if (typeOfTicketSelectedIndex > -1)
                    cell.detailTextLabel.text = [[self.tiposDeIngressos objectAtIndex:typeOfTicketSelectedIndex]titulo];
                else
                    cell.detailTextLabel.text = @"";
            }
            else
                cell.detailTextLabel.text = @"";
            
            break;
            
        case 1:
            cell.textLabel.text = @"Quantidade";
            if (numberOfTicketsSelectedIndex >= 0)
                cell.detailTextLabel.text = [searchNumberOfTyckets objectAtIndex:numberOfTicketsSelectedIndex];
            else
                cell.detailTextLabel.text = @"";
            break;
            
        default:
            break;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (pickerOnScreen){
        [self dismissPickerView];
        return;
    }
    
    if (indexPath.section == 1) {
        if (typeOfTicketSelectedIndex == -1 || numberOfTicketsSelectedIndex == -1 || self.tiposDeIngressos.count == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao adicionar ao carrinho."
                                                            message:@"Por favor, preencha todos os campos informados para prosseguir."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        int numberOfTickets = [[searchNumberOfTyckets objectAtIndex:numberOfTicketsSelectedIndex] intValue];
        numberOfTicketsInQueue = numberOfTickets;
        for (int i = 0; i < numberOfTickets; i++) {
            Ingresso *ticket = [Ingresso object];
            ticket.conteudoId = self.conteudo.conteudoId;
            ticket.tipoDeIngressoId = [[self.tiposDeIngressos objectAtIndex:typeOfTicketSelectedIndex] tiposDeIngressosId];
            
            //[SVProgressHUD show];
            
            [objectManager postObject:ticket delegate:self];
            [HUD showUIBlockingIndicatorWithText:@"Adicionando ao carrinho..."];
        }
        return;
    }
    
    
    CGRect pickerFrame = CGRectMake(0,self.view.frame.size.height - 220, self.view.frame.size.width, 180);
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    [pickerView setTag:indexPath.row];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    if (indexPath.row == 0) {
        if (typeOfTicketSelectedIndex == -1) {
            typeOfTicketSelectedIndex = 0;
        }
        [pickerView selectRow:typeOfTicketSelectedIndex inComponent:0 animated:NO];
    }
    else{
        if (numberOfTicketsSelectedIndex == -1) {
            numberOfTicketsSelectedIndex = 0;
        }
        [pickerView selectRow:numberOfTicketsSelectedIndex inComponent:0 animated:NO];
    }
    
    toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.frame = CGRectMake(0, self.view.frame.size.height - 40,self.view.frame.size.width , 40);
    closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Ok"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(self.view.frame.size.width - 57, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventValueChanged];
    [toolbar addSubview:closeButton];
    
    [self.view addSubview:toolbar];
    [self.view addSubview:pickerView];
    //
    
    pickerOnScreen = TRUE;
}

-(void)dismissPickerView{
    [pickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    pickerOnScreen = FALSE;
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    [self.tableView reloadData];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)picker numberOfRowsInComponent:(NSInteger)component {
    if (picker.tag == 0){
        return self.tiposDeIngressos.count;
        NSIndexPath *index = [[NSIndexPath alloc] init];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:index];
        cell.detailTextLabel.text = [[self.tiposDeIngressos objectAtIndex:typeOfTicketSelectedIndex]titulo];
    }
    
    else
        return searchNumberOfTyckets.count;
    
}

- (NSString *)pickerView:(UIPickerView *)picker titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (picker.tag == 0)
        return [[self.tiposDeIngressos objectAtIndex:row] titulo];
    else
        return [searchNumberOfTyckets objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)picker didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (picker.tag == 0) {
        typeOfTicketSelectedIndex = row;
        
    }
    else{
        numberOfTicketsSelectedIndex = row;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
}

#pragma mark Restkit methods

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    numberOfTicketsInQueue--;
    
    
    if (numberOfTicketsInQueue == 0) {
        [HUD hideUIBlockingIndicator];
        [[self.navigationController.viewControllers objectAtIndex:0] handleShowCart];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    [HUD hideUIBlockingIndicator];
}


- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
            
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            
            
        }
        
    } else if ([request isDELETE]) {
        
        // Handling DELETE /missing_resource.txt
        if ([response isNotFound]) {
            
        }
    }
}

- (void)request:(RKRequest *)request didFailAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
}

@end
