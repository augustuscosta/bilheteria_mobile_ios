//
//  CartCell.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 26/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *ShowName;
@property (strong, nonatomic) IBOutlet UILabel *ShowDate;
@property (strong, nonatomic) IBOutlet UILabel *ShowTime;
@property (strong, nonatomic) IBOutlet UILabel *TicketType;
@property (strong, nonatomic) IBOutlet UILabel *TicketValue;


@end
