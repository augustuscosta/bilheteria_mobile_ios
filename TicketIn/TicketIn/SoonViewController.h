//
//  SoonViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"

@protocol SoonViewControllerDelegate <NSObject>

-(void)handleSelectRowOnSoonEvents:(Conteudo *)conteudo;

@end

@interface SoonViewController : UITableViewController{
    id <SoonViewControllerDelegate>delegate;
}

@property (nonatomic, retain) id <SoonViewControllerDelegate>delegate;

@property (strong, nonatomic) NSMutableArray* allTableData;
@property (strong, nonatomic) NSMutableArray* filteredTableData;

@property (nonatomic, retain) NSMutableArray *soonConteudos;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) bool isFiltered;


@end
