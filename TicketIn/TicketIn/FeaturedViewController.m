//
//  FeaturedViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 16/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "FeaturedViewController.h"
#import "InfinitePagingView.h"
#import "Conteudo.h"
#import "EventImageView.h"

@interface FeaturedViewController () <InfinitePagingViewDelegate>

@end

@implementation FeaturedViewController
{
    UIPageControl *pageControl;
    InfinitePagingView *pagingView;
}

@synthesize delegate;
@synthesize arrayWithFeaturedObjects = _arrayWithFeaturedObjects;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initPagingView];
    [self loadPages];
    
    UITapGestureRecognizer *touchInFeaturedObject = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchOnScreen)];
    [self.view addGestureRecognizer:touchInFeaturedObject];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //autoTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:pagingView selector:@selector(scrollToNextPage) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [autoTimer invalidate];
    autoTimer = nil;
}

-(void)handleTouchOnScreen{
    if ([delegate respondsToSelector:@selector(handleTapGestureOnFeaturedObject:)]) {
        [delegate handleTapGestureOnFeaturedObject:[self.arrayWithFeaturedObjects objectAtIndex:pageControl.currentPage]];
    }
}

-(UIImage *)loadImage:(int)atIndex{
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[[self.arrayWithFeaturedObjects objectAtIndex:atIndex] imagemUrl]]];
    UIImage *imageToReturn = [UIImage imageWithData:imageData];
    
    return [self imageWithImage:imageToReturn convertToSize:pagingView.frame.size];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(void)loadPages{
    for (NSUInteger i = 0; i < self.arrayWithFeaturedObjects.count; i++) {
        EventImageView *page = [[EventImageView alloc] initWithFrame:pagingView.frame url:[NSURL URLWithString:[[self.arrayWithFeaturedObjects objectAtIndex:i] imagemUrl]]];
        page.index = i;
        page.contentMode = UIViewContentModeScaleAspectFit;
        [pagingView addPageView:page];
    }
    if (self.arrayWithFeaturedObjects.count == 2) {
        for (NSUInteger i = 0; i < self.arrayWithFeaturedObjects.count; i++) {
            EventImageView *page = [[EventImageView alloc] initWithFrame:pagingView.frame url:[NSURL URLWithString:[[self.arrayWithFeaturedObjects objectAtIndex:i] imagemUrl]]];
            page.index = 2 + i;
            page.contentMode = UIViewContentModeScaleAspectFit;
            [pagingView addPageView:page];
        }
    }
}

-(void)initPagingView{
    
    [self.view setFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - 61)];
    pagingView = [[InfinitePagingView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - 30)];
    pagingView.delegate = self;
    [self.view addSubview:pagingView];
    [self initPageControl];
}

-(void)initPageControl{
    pageControl = [[UIPageControl alloc] init];
    pageControl.center = CGPointMake(self.view.center.x, pagingView.frame.size.height + 15.f);
    pageControl.numberOfPages = self.arrayWithFeaturedObjects.count;
    [self.view addSubview:pageControl];
}

#pragma mark - InfinitePagingViewDelegate


- (void)pagingView:(InfinitePagingView *)pv didEndDecelerating:(UIScrollView *)scrollView atPageIndex:(NSInteger)pageIndex
{
    CGRect r = [[[[pagingView.subviews objectAtIndex:0] subviews] objectAtIndex:0] frame];
    if (r.origin.x == 0) {
        pageIndex = 0;
    }
    else{
        float numberOfPages;
        if(self.arrayWithFeaturedObjects.count == 2) numberOfPages = self.arrayWithFeaturedObjects.count + 2;
        else    numberOfPages = self.arrayWithFeaturedObjects.count;
        float page = ((numberOfPages * 320) - r.origin.x)/320;
        pageIndex = page;
    }
    
    if (self.arrayWithFeaturedObjects.count == 2) {
        if (pageIndex == 2)
            pageIndex = 0;
        if (pageIndex == 3)
            pageIndex = 1;
    }
    
    
    pageControl.currentPage = pageIndex;
    
    
}

- (void)hudWasHidden{
    
}

@end