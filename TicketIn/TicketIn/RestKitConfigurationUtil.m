    //
//  RestKitConfigurationUtil.m
//  skynet
//
//  Created by Augustus Costa on 3/1/12.
//  Copyright (c) 2012 AfterSix. All rights reserved.
//

#import "RestKitConfigurationUtil.h"
#import "Conteudo.h"
#import "TiposDeIngressos.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "Session.h"
#import "Usuario.h"
#import "Ingresso.h"

@interface RestKitConfigurationUtil()

-(void)setupRestKit;

@end

@implementation RestKitConfigurationUtil

-(void)initRestKit{
    [self setupRestKit];
}

-(void)setupRestKit{
    RKObjectManager* objectManager = [RKObjectManager objectManagerWithBaseURLString:@"http://23.23.89.95:3001"];
    objectManager.serializationMIMEType = RKMIMETypeJSON;
    objectManager.client.disableCertificateValidation = YES; // Using self signed cert
    objectManager.client.cachePolicy = RKRequestCachePolicyLoadIfOffline |
    RKRequestCachePolicyTimeout |
    RKRequestCachePolicyLoadOnError |
    RKRequestCachePolicyEtag;
    objectManager.client.requestCache.storagePolicy = RKRequestCacheStoragePolicyPermanently;
    objectManager.client.requestQueue.showsNetworkActivityIndicatorWhenBusy = YES;
    objectManager.objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:[NSString stringWithFormat:@"ticktin.sqlite"] usingSeedDatabaseName:nil managedObjectModel:nil delegate:self];
    objectManager.objectStore.cacheStrategy = [RKInMemoryManagedObjectCache new];
    
    RKManagedObjectMapping *ticketTypeMapping = [RKManagedObjectMapping mappingForClass:[TiposDeIngressos class] inManagedObjectStore:objectManager.objectStore];
    [ticketTypeMapping setPrimaryKeyAttribute:@"tiposDeIngressosId"];
    [ticketTypeMapping mapKeyPathsToAttributes:@"id", @"tiposDeIngressosId",
     @"created_at", @"createdAt",
     @"descricao", @"descricao",
     @"empresa_id", @"empresaId",
     @"quantidade_ingressos", @"quantidadeIngressos",
     @"titulo", @"titulo",
     @"updated_at", @"updatedAt",
     @"valor", @"valor",
     nil];
    
    RKManagedObjectMapping* conteudoMapping = [RKManagedObjectMapping mappingForClass:[Conteudo class] inManagedObjectStore:objectManager.objectStore];
    conteudoMapping.primaryKeyAttribute = @"conteudoId";
    conteudoMapping.rootKeyPath = @"conteudo";
    [conteudoMapping mapKeyPathsToAttributes:@"id", @"conteudoId",
     @"created_at", @"createdAt",
     @"updated_at", @"updatedAt",
     @"data", @"data",
     @"data_fim_vendas", @"dataFimVendas",
     @"data_incio_vendas", @"dataInicioVendas",
     @"ativo", @"ativo",
     @"categoria", @"categoria",
     @"descricao", @"descricao",
     @"destaque", @"destaque",
     @"empresa_id", @"empresaId",
     @"endereco", @"endereco",
     @"imagem_content_type", @"imagemContentType",
     @"imagem_file_name", @"imagemFileName",
     @"imagem_file_size", @"imagemFileSize",
     @"imagem_updated_at", @"imagemUpdatedAt",
     @"latitude", @"latitude",
     @"longitude", @"longitude",
     @"parent_id", @"parentId",
     @"row_imagem_content_type", @"rowImagemContentType",
     @"row_imagem_file_name", @"rowImagemFileName",
     @"row_imagem_file_size", @"rowImagemFileSize",
     @"row_imagem_updated_at", @"rowImagemUpdatedAt",
     @"social", @"social",
     @"subtitulo", @"subtitulo",
     @"titulo", @"titulo",
     @"url", @"url",
     @"imagem_url", @"imagemUrl",
     @"imagem_thumb_url", @"imagemThumbUrl",
     @"imagem_medium_url", @"imagemMediumUrl",
     @"imagem_large_url", @"imagemLargeUrl",
     @"imagem_icon_url", @"imagemIconUrl",
     @"row_imagem_url", @"rowImagemUrl",
     @"row_imagem_thumb_url", @"rowImagemThumbUrl",
     @"row_imagem_medium_url", @"rowImagemMediumUrl",
     @"row_imagem_large_url", @"rowImagemLargeUrl",
     nil];
    [conteudoMapping mapKeyPath:@"tipo_de_ingressos" toRelationship:@"tiposDeIngressos" withMapping:ticketTypeMapping];
    
    //[objectManager.mappingProvider setMapping:conteudoMapping forKeyPath:@"conteudo"];
    
    //[objectManager.mappingProvider setObjectMapping:conteudoMapping forKeyPath:@"conteudo"];
    [objectManager.mappingProvider setObjectMapping:conteudoMapping
                             forResourcePathPattern:@"/empresas/conteudo/all"
                              withFetchRequestBlock:^NSFetchRequest *(NSString *resourcePath) {
                                  return [Conteudo fetchRequest];
                              }];
    
    RKObjectMapping *sessionMapping = [RKObjectMapping mappingForClass:[Session class]];
    [sessionMapping mapKeyPathsToAttributes:@"password", @"password",
     @"email", @"email",
     nil];
    sessionMapping.rootKeyPath = @"session";
    [objectManager.mappingProvider setSerializationMapping:sessionMapping forClass:[Session class]];
    //[objectManager.mappingProvider setMapping:sessionMapping forKeyPath:@"session"];
    
    RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:[Usuario class]];
    [userMapping mapKeyPathsToAttributes:
     @"estado", @"state",
     @"nascimento", @"birthdate",
     @"cidade", @"city",
     @"cep", @"cep",
     @"endereco", @"adress",
     @"telefone", @"phone",
     @"password", @"password",
     @"email", @"email",
     @"rg", @"rg",
     @"cpf", @"cpf",
     @"nome", @"name",
     nil];
   // NSString *weirdEmptyStringWhereNilJustFails = @"";
    //[[RKObjectManager sharedManager].mappingProvider registerObjectMapping:userMapping withRootKeyPath: weirdEmptyStringWhereNilJustFails];
    //userMapping.rootKeyPath = @"";
    [objectManager.mappingProvider setSerializationMapping:[userMapping inverseMapping] forClass:[Usuario class]];
    [objectManager.mappingProvider setMapping:userMapping forKeyPath:@"usuario"];
    
    RKManagedObjectMapping* ticketMapping = [RKManagedObjectMapping mappingForClass:[Ingresso class] inManagedObjectStore:objectManager.objectStore];
    ticketMapping.primaryKeyAttribute = @"ingressoId";
    ticketMapping.rootKeyPath = @"ingresso";
    [ticketMapping mapKeyPathsToAttributes:@"id", @"ingressoId",
     @"created_at", @"createdAt",
     @"usuario_id", @"usuarioId",
     @"conteudo_id", @"conteudoId",
     @"tipo_de_ingresso_id", @"tipoDeIngressoId",
     @"transacao", @"transacao",
     @"resposta", @"resposta",
     nil];
    [objectManager.mappingProvider setSerializationMapping:[ticketMapping inverseMapping] forClass:[Ingresso class]];
    //[objectManager.mappingProvider setMapping:ticketMapping forKeyPath:@"ingresso"];
    
    [objectManager.mappingProvider setObjectMapping:ticketMapping
                             forResourcePathPattern:@"/ingressos"
                              withFetchRequestBlock:^NSFetchRequest *(NSString *resourcePath) {
                                  return [Ingresso fetchRequest];
                              }];
    
    RKObjectRouter *sessionRouter = [RKObjectManager sharedManager].router;
    [sessionRouter routeClass:[Session class] toResourcePath:@"/sessions" forMethod:RKRequestMethodPOST];
    
    RKObjectRouter *userRouter = [RKObjectManager sharedManager].router;
    [userRouter routeClass:[Usuario class] toResourcePath:@"/usuarios" forMethod:RKRequestMethodPOST];
    
    RKObjectRouter *tickerRouterPOST = [RKObjectManager sharedManager].router;
    [tickerRouterPOST routeClass:[Ingresso class] toResourcePath:@"/ingressos" forMethod:RKRequestMethodPOST];
    
    RKObjectRouter *tickerRouterDELETE = [RKObjectManager sharedManager].router;
    [tickerRouterDELETE routeClass:[Ingresso class] toResourcePath:@"/ingressos" forMethod:RKRequestMethodDELETE];
    
    RKObjectRouter *tickerRouterPUT = [RKObjectManager sharedManager].router;
    [tickerRouterPUT routeClass:[Ingresso class] toResourcePath:@"/ingressos" forMethod:RKRequestMethodPUT];
    
    
    // Set it Globally
    [RKObjectMapping addDefaultDateFormatterForString:@"yyyy-MM-dd'T'HH:mm:ssZ" inTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
}

@end
