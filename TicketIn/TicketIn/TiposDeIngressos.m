//
//  TiposDeIngressos.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "TiposDeIngressos.h"


@implementation TiposDeIngressos

@dynamic createdAt;
@dynamic descricao;
@dynamic empresaId;
@dynamic quantidadeIngressos;
@dynamic tiposDeIngressosId;
@dynamic titulo;
@dynamic updatedAt;
@dynamic valor;
@dynamic conteudo;

@end
