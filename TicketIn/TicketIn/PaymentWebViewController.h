//
//  PaymentWebViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 25/06/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentWebViewDelegate <NSObject>

-(void)handleTicketsPayment:(NSString *)transactionId;

@end

@interface PaymentWebViewController : UIViewController<UIWebViewDelegate>{
    UIImageView *pagSeguroBanner;
    Boolean *firtTimeWeb;
    id <PaymentWebViewDelegate>delegate;
}
@property (nonatomic, retain) id <PaymentWebViewDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSArray *products;

@end
