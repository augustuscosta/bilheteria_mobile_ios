//
//  LoginViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 23/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "LoginViewController.h"
#import "Session.h"
#import "Empresa.h"
#import "Conteudo.h"
#import "TiposDeIngressos.h"
#import "Usuario.h"
#import "RestKitConfigurationUtil.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize loginTextField = _loginTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize missPasswordButton = _missPasswordButton;
@synthesize theNewUserButton = _theNewUserButton;
@synthesize loginButton = _loginButton;
@synthesize scrollView = _scrollView;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;
@synthesize backgroundImageView = _backgroundImageView;
@synthesize poweredImageView = _poweredImageView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self.poweredImageView removeFromSuperview];
    
    
    [self.missPasswordButton setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;

}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden: YES animated:NO];
    
    objectManager = [RKObjectManager sharedManager];
    NSString *cookie = [[NSUserDefaults standardUserDefaults] stringForKey:@"Cookie"];
    
    if (cookie) {
        [objectManager.client setValue:cookie forHTTPHeaderField:@"Cookie"];
        [self performSegueWithIdentifier:@"LoginSegue" sender:self];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.scrollView setContentSize:self.view.frame.size];
    
    UIImage *poweredImage = [UIImage imageNamed:@"powered.png"];
    
    CGSize poweredImageFrame = CGSizeMake(poweredImage.size.width, poweredImage.size.height);
    UIImageView *poweredView = [[UIImageView alloc] initWithImage:poweredImage];
    CGRect poweredViewFrame = CGRectMake(self.view.frame.size.width/2 - poweredImageFrame.width/4, self.view.frame.size.height - poweredImageFrame.height/2 - 10, poweredImageFrame.width/2, poweredImageFrame.height/2);
    [poweredView setFrame:poweredViewFrame];
    
    [self.scrollView addSubview:poweredView];
    
    [self.backgroundImageView removeFromSuperview];
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.jpg"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.scrollView.frame.size];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.scrollView addSubview:self.backgroundImageView];
    [self.scrollView sendSubviewToBack:self.backgroundImageView];
    
    NSString *login = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserLogin"];
    self.loginTextField.text = login;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)doneClicked:(UIButton *)sender {
    
    if([self.loginTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao logar"
                                                        message:@"Não foi possível fazer login com o servidor. O campo Usuário encontra-se vazio."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    if ([self.passwordTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao logar"
                                                        message:@"Não foi possível fazer login com o servidor. O campo Senha encontra-se vazio."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }

    [HUD showUIBlockingIndicatorWithText:@"Acessando Sistema..."];
    [self login];
    
    
}

- (void)login{
    // Define a default resource path for all unspecified HTTP verbs
    Session *session = [[Session alloc] init];
    session.email = self.loginTextField.text;
    session.password = self.passwordTextField.text;
    [objectManager postObject:session delegate:self];
    
    
}

- (IBAction)NewUserButtonClicked:(UIButton *)sender {
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height += (keyboardSize.height);
    else
        viewFrame.size.height += (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
    
    [self performSelector:@selector(adjustScrollviewContentSize:) withObject:[NSNumber numberWithFloat:self.view.frame.size.height] afterDelay:0.1 ];
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height -= (keyboardSize.height);
    else
        viewFrame.size.height -= (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    [self.scrollView setContentOffset:CGPointMake (0, 0) animated: NO];
    
    keyboardIsShown = YES;
    if(self.loginTextField.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.loginTextField.frame.origin.y - self.loginTextField.bounds.size.height/2)/2) animated: NO];
    else
        [self.scrollView setContentOffset:CGPointMake(0, (self.passwordTextField.frame.origin.y - self.passwordTextField.bounds.size.height/2)/2) animated: NO];
    
    [self performSelector:@selector(adjustScrollviewContentSize:) withObject:[NSNumber numberWithFloat:(viewFrame.size.height + (keyboardSize.height)) ] afterDelay:0.1 ];
}

-(void)adjustScrollviewContentSize:(NSNumber *)height{
    [self.scrollView setContentSize:CGSizeMake(320, [height floatValue])];
}


#pragma mark UITextFieldDelegate methods


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.loginTextField) {
        [textField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    
    if(textField.returnKeyType == UIReturnKeyDone){
        [textField resignFirstResponder];
        [self doneClicked:self.loginButton];
    }
    
    return YES;
}

#pragma mark RKObjectLoaderDelegate methods

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    [HUD hideUIBlockingIndicator];
    
    NSString *cookie;
    
    switch ([[objectLoader response] statusCode]) {
        case 200:
            cookie = [[[objectLoader response] allHeaderFields] valueForKey:@"Set-Cookie"];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:cookie forKey:@"Cookie"];
            break;
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:self.loginTextField.text forKey:@"UserLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"LoginSegue" sender:self];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    
    [HUD hideUIBlockingIndicator];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao logar"
                                                    message:@"Não foi possível fazer login com o servidor. Verifique seu usuário e senha. Caso não haja erro, contacte o responsável pelo servidor."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex {
    if (buttonIndex == 0) {
        //[self.HUD removeFromSuperview];
    }
}

#pragma mark RKRequestDelegate methods

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
            // Success! Let's take a look at the data
            NSLog(@"Retrieved XML: %@", [response bodyAsString]);
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            NSLog(@"JSON Response from Server %@", [response bodyAsString] );
            
        }
        
    } else if ([request isDELETE]) {
        
        // Handling DELETE /missing_resource.txt
        if ([response isNotFound]) {
            NSLog(@"The resource path '%@' was not found.", [request resourcePath]);
        }
    }
}

- (void)request:(RKRequest *)request didFailAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
}

@end
