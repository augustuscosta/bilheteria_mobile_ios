//
//  EventDetailViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import "purchaseOptionsTableViewController.h"

@interface EventDetailViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIWebViewDelegate>{
    UIImage *img;
}

@property (nonatomic, retain)Conteudo *conteudo;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) UIButton *BuyButton;
@property (nonatomic, retain) UIWebView *webView;

@property (nonatomic, retain) UICollectionView *collectionView;

@end
