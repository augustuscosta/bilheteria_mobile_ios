//
//  TiposDeIngressos.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Conteudo;

@interface TiposDeIngressos : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSString * empresaId;
@property (nonatomic, retain) NSNumber * quantidadeIngressos;
@property (nonatomic, retain) NSNumber * tiposDeIngressosId;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSNumber * valor;
@property (nonatomic, retain) NSSet *conteudo;
@end


