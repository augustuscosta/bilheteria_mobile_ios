//
//  ContainerViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 14/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "ContainerViewController.h"
#import "FeaturedViewController.h"
#import "EventDetailViewController.h"
#import "LegalViewController.h"
#import "PaymentWebViewController.h"
#import "RestKitConfigurationUtil.h"


@interface ContainerViewController ()

@end

@implementation ContainerViewController

@synthesize allConteudos;
@synthesize backgroundView = _backgroundView;


- (void)viewDidLoad
{
    loadingHome = TRUE;
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doUpdate)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    objectManager = [RKObjectManager sharedManager];
    [self.backgroundView setFrame:CGRectMake(0, 0, 200, 200)];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
}

#pragma mark private methods

-(void)doUpdate{
    [fVC.view removeFromSuperview];
    fVC = nil;
    [self loadConteudo];
}

-(void)handleShowCart{
    [self cartButtonPressed:self];
}

-(void)loadConteudo{
    NSString *path = @"/empresas/conteudo/all";	
    [objectManager loadObjectsAtResourcePath:path delegate:self];
}

-(void)loadHomePage{
    NSMutableArray *featuredConteudos = [[NSMutableArray alloc] init];
    for (Conteudo *conteudo in self.allConteudos) {
        
        if ([conteudo.destaque isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            [featuredConteudos addObject:conteudo];
        }
    }
    fVC = [[FeaturedViewController alloc] init];
    fVC.arrayWithFeaturedObjects = featuredConteudos;
    fVC.delegate = self;
    [self addChildViewController:fVC];
    [self.view addSubview:fVC.view];
    
    vcInScene = fVC;
    loadingHome = FALSE;
    
    logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutButton addTarget:self
                     action:@selector(logoutButtonPressed)
           forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setBackgroundImage:[UIImage imageNamed:@"logout2.png"] forState:UIControlStateNormal];
    logoutButton.frame = CGRectMake(3.0, 3.0, 30.0, 30.0);
    [self.view addSubview:logoutButton];
}

- (IBAction)featuredButtonPressed:(UIButton *)sender {
    if (loadingHome) {
        return;
    }
    if (![vcInScene isKindOfClass:[FeaturedViewController class]]) {
        [self transitionFromViewController:vcInScene
                          toViewController:fVC
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionNone
                                animations:nil
                                completion:nil];
        vcInScene = fVC;
        [logoutButton removeFromSuperview];
        [self.view addSubview:logoutButton];
    }
}
- (IBAction)inTheatersButtonPressed:(UIButton *)sender {
    if (loadingHome) {
        return;
    }
    if (!inTheatersViewLoaded) {
        NSMutableArray *inTheatersConteudos = [[NSMutableArray alloc] init];
        for (Conteudo *conteudo in self.allConteudos) {
            NSDate *conteudoInicioVendasDate = conteudo.dataInicioVendas;
            NSDate *conteudoFimVendasDate = conteudo.dataFimVendas;
            NSDate *deviceCurrentDate = [NSDate date];
            NSComparisonResult comparisonInicioVendas = [deviceCurrentDate compare:conteudoInicioVendasDate];
            NSComparisonResult comparisonFimVendas = [deviceCurrentDate compare:conteudoFimVendasDate];
            
            if (comparisonInicioVendas != NSOrderedAscending && comparisonFimVendas == NSOrderedAscending)
                [inTheatersConteudos addObject:conteudo];
        }
        iTVC = [[InTheatersViewController alloc] init];
        iTVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InTheaters"];
        iTVC.delegate = self;
        iTVC.inTheatersConteudos = inTheatersConteudos;
        [self addChildViewController:iTVC];
        inTheatersViewLoaded = YES;
        
    }
    if (![vcInScene isKindOfClass:[InTheatersViewController class]]) {
        [self transitionFromViewController:vcInScene
                          toViewController:iTVC
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionNone
                                animations:nil
                                completion:nil];
        vcInScene = iTVC;
    }
}
- (IBAction)soonButtonPressed:(id)sender {
    if (loadingHome) {
        return;
    }
    if (!soonViewLoaded) {
        NSMutableArray *soonConteudos = [[NSMutableArray alloc] init];
        for (Conteudo *conteudo in self.allConteudos) {
            NSDate *conteudoInicioVendasDate = conteudo.dataInicioVendas;
            NSDate *deviceCurrentDate = [NSDate date];
            NSDate *conteudoFimVendasDate = conteudo.dataFimVendas;
            NSComparisonResult comparisonInicioVendas = [deviceCurrentDate compare:conteudoInicioVendasDate];
            NSComparisonResult comparisonFimVendas = [deviceCurrentDate compare:conteudoFimVendasDate];
            
            if (comparisonInicioVendas == NSOrderedAscending && comparisonFimVendas == NSOrderedAscending)
                [soonConteudos addObject:conteudo];
        }
        sVC = [[SoonViewController alloc] init];
        sVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Soon"];
        sVC.delegate = self;
        sVC.soonConteudos = soonConteudos;
        [self addChildViewController:sVC];
        soonViewLoaded = YES;
        
    }
    if (![vcInScene isKindOfClass:[SoonViewController class]]) {
        [self transitionFromViewController:vcInScene
                          toViewController:sVC
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionNone
                                animations:nil
                                completion:nil];
        vcInScene = sVC;
    }
}
- (IBAction)ticketsButtonPressed:(UIButton *)sender {
    if (loadingHome) {
        return;
    }
    if(!ticketsViewLoaded) {
        lTVC = [[ListOfTicketsViewController alloc] init];
        lTVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ListOfTicketsView"];
        lTVC.conteudos = self.allConteudos;
        [self addChildViewController:lTVC];
    }
    if (![vcInScene isKindOfClass:[ListOfTicketsViewController class]]) {
        [self transitionFromViewController:vcInScene
                          toViewController:lTVC
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionNone
                                animations:nil
                                completion:nil];
        vcInScene = lTVC;
    }
}

- (IBAction)cartButtonPressed:(id)sender {
    if(!cartViewLoaded) {
        cVC = [[CartViewController alloc] init];
        cVC.delegate = self;
        cVC.conteudos = self.allConteudos;
        [self addChildViewController:cVC];
    }
    if (![vcInScene isKindOfClass:[CartViewController class]]) {
        [self transitionFromViewController:vcInScene
                          toViewController:cVC
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionNone
                                animations:nil
                                completion:nil];
        vcInScene = cVC;
    }
}


#pragma mark inTheatersViewControllerDelegate methods

-(void)handleTapGestureOnFeaturedObject:(Conteudo *)conteudo{
    featuredConteudo = conteudo;
    [self performSegueWithIdentifier:@"EventDetailSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EventDetailSegue"]) {
        EventDetailViewController *eDVC = segue.destinationViewController;
        eDVC.conteudo = featuredConteudo;
    }
}

#pragma mark SoonViewControllerDelegate methods

-(void)handleSelectRowOnSoonEvents:(Conteudo *)conteudo{
    EventDetailViewController *eDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetail"];
    eDVC.conteudo = conteudo;
    [self.navigationController pushViewController:eDVC animated:YES];
}

#pragma mark InTheatersViewControllerDelegate methods

-(void)handleSelectRowOnInTheatersEvents:(Conteudo *)conteudo{
    EventDetailViewController *eDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetail"];
    eDVC.conteudo = conteudo;
    [self.navigationController pushViewController:eDVC animated:YES];
}

#pragma mark CartViewControllerDelegate methods

-(void)handleOpenTAU{
    LegalViewController *TAUVc = [[LegalViewController alloc] init];
    [self.navigationController pushViewController:TAUVc animated:YES];
}

-(void)handleOpenPayment:(NSArray *)products{
    PaymentWebViewController *PWVC = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentViewController"];
    [PWVC setProducts:products];
    PWVC.delegate = cVC;
    [self.navigationController pushViewController:PWVC animated:YES];
}

-(void)loadObjectFromDatabase{
    NSFetchRequest* request = [Conteudo fetchRequest];
	NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    NSArray *array = [[NSArray alloc] init];
    array = [Conteudo objectsWithFetchRequest:request];
    self.allConteudos = [Conteudo organizeData:array];
    
    [self loadHomePage];
    
}

#pragma mark RKRequestDelegate methods

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastUpdatedAt"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self loadObjectFromDatabase];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self loadObjectFromDatabase];
}

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
            // Success! Let's take a look at the data
            NSLog(@"Retrieved XML: %@", [response bodyAsString]);
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            NSLog(@"JSON Response from Server %@", [response bodyAsString] );
            
        }
        
    } else if ([request isDELETE]) {
        
        // Handling DELETE /missing_resource.txt
        if ([response isNotFound]) {
            NSLog(@"The resource path '%@' was not found.", [request resourcePath]);
        }
    }
}

- (void)request:(RKRequest *)request didFailAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
}

-(void)logoutButtonPressed{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sair"
                                                    message:@"Você está prestes a realizar o Log Out da aplicação. Você tem certeza que deseja continuar?"
                                                   delegate:self
                                          cancelButtonTitle:@"Não"
                                          otherButtonTitles: @"Sim", nil];
    [alert show];
}

-(void)logout{
    [[RKClient sharedClient].requestCache invalidateAll];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"ticketin.sqlite"];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    [[objectManager.client requestCache] invalidateAll];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Cookie"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==0)
    {
        NSLog(@"Log Out Cancelado");
    }
    else
    {
        [self logout];
    }
    
}


@end
