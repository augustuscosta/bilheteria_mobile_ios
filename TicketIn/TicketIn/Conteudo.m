//
//  Conteudo.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Conteudo.h"


@implementation Conteudo

@dynamic ativo;
@dynamic categoria;
@dynamic conteudoId;
@dynamic createdAt;
@dynamic data;
@dynamic dataFimVendas;
@dynamic dataInicioVendas;
@dynamic descricao;
@dynamic destaque;
@dynamic empresaId;
@dynamic endereco;
@dynamic imagemContentType;
@dynamic imagemFileName;
@dynamic imagemFileSize;
@dynamic imagemIconUrl;
@dynamic imagemLargeUrl;
@dynamic imagemMediumUrl;
@dynamic imagemThumbUrl;
@dynamic imagemUpdatedAt;
@dynamic imagemUrl;
@dynamic latitude;
@dynamic longitude;
@dynamic parentId;
@dynamic rowImagemContentType;
@dynamic rowImagemFileName;
@dynamic rowImagemFileSize;
@dynamic rowImagemLargeUrl;
@dynamic rowImagemMediumUrl;
@dynamic rowImagemThumbUrl;
@dynamic rowImagemUpdatedAt;
@dynamic rowImagemUrl;
@dynamic social;
@dynamic subtitulo;
@dynamic titulo;
@dynamic updatedAt;
@dynamic url;
@dynamic tiposDeIngressos;
@synthesize conteudos;
@synthesize childs;

+(NSMutableArray *)organizeData:(NSArray*)conteudos
{
    if(conteudos == nil)
        return nil;
    NSMutableArray *roots = [[NSMutableArray alloc] init];
    
    for(Conteudo *conteudo in conteudos){
        if (conteudo.parentId == nil)
            [roots addObject:conteudo];
        conteudo.childs = [Conteudo findChilds:conteudos toParent:[conteudo conteudoId]];
    }
    return roots;
}

+(NSMutableArray *)findChilds:(NSArray*)conteudos toParent:(NSNumber *)rootId{
    if(conteudos == nil)
        return nil;
    NSMutableArray *toReturn = [[NSMutableArray alloc] init];
    for(Conteudo *conteudo in conteudos){
        if([conteudo.parentId isEqualToNumber:rootId])
            [toReturn addObject:conteudo];
    }
    return toReturn;
}

@end
