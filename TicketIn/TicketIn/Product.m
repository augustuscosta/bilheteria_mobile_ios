//
//  Product.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 25/06/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize productID, productName, productValue;

@end
