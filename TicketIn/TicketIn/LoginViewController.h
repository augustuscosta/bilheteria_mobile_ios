//
//  LoginViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 23/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "Session.h"
#import "HUD.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate, RKRequestDelegate, RKObjectLoaderDelegate, UIAlertViewDelegate>{
    BOOL keyboardIsShown;
    RKObjectManager* objectManager;
}

@property (strong, nonatomic) IBOutlet UITextField *loginTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *missPasswordButton;
@property (strong, nonatomic) IBOutlet UIButton *theNewUserButton;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImage *backgroundImageOriginal;
@property (strong, nonatomic) UIImage *backgroundImageResized;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *poweredImageView;


@end
