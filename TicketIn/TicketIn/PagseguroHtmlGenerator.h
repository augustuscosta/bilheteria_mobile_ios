//
//  PagseguroHtmlGenerator.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 24/04/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PagseguroHtmlGenerator : NSObject


-(NSString *)returnHtml:(NSArray *)itensId iDescription:(NSArray*)itensDescription iAmount:(NSArray *)itensAmount;

@end
