//
//  Conteudo.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TiposDeIngressos;

@interface Conteudo : NSManagedObject

@property (nonatomic, retain) NSNumber * ativo;
@property (nonatomic, retain) NSString * categoria;
@property (nonatomic, retain) NSNumber * conteudoId;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * data;
@property (nonatomic, retain) NSDate * dataFimVendas;
@property (nonatomic, retain) NSDate * dataInicioVendas;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSNumber * destaque;
@property (nonatomic, retain) NSNumber * empresaId;
@property (nonatomic, retain) NSString * endereco;
@property (nonatomic, retain) NSString * imagemContentType;
@property (nonatomic, retain) NSString * imagemFileName;
@property (nonatomic, retain) NSNumber * imagemFileSize;
@property (nonatomic, retain) NSString * imagemIconUrl;
@property (nonatomic, retain) NSString * imagemLargeUrl;
@property (nonatomic, retain) NSString * imagemMediumUrl;
@property (nonatomic, retain) NSString * imagemThumbUrl;
@property (nonatomic, retain) NSDate * imagemUpdatedAt;
@property (nonatomic, retain) NSString * imagemUrl;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * parentId;
@property (nonatomic, retain) NSString * rowImagemContentType;
@property (nonatomic, retain) NSString * rowImagemFileName;
@property (nonatomic, retain) NSNumber * rowImagemFileSize;
@property (nonatomic, retain) NSString * rowImagemLargeUrl;
@property (nonatomic, retain) NSString * rowImagemMediumUrl;
@property (nonatomic, retain) NSString * rowImagemThumbUrl;
@property (nonatomic, retain) NSDate * rowImagemUpdatedAt;
@property (nonatomic, retain) NSString * rowImagemUrl;
@property (nonatomic, retain) NSNumber * social;
@property (nonatomic, retain) NSString * subtitulo;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *tiposDeIngressos;
@property (nonatomic, retain) NSMutableArray *conteudos;
@property (nonatomic, retain) NSMutableArray *childs;

+(NSMutableArray *)organizeData:(NSArray*)conteudos;
+(NSMutableArray *)findChilds:(NSArray*)conteudos toParent:(NSNumber *)rootId;

@end

