//
//  CadastreViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CadastreViewController.h"
#import "CadastreRowInfo.h"
#import "ItemRegistrationCell.h"
#import "ValidateCadastreCell.h"
#import "Usuario.h"

@interface CadastreViewController ()

@end

@implementation CadastreViewController

@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    objectManager = [RKObjectManager sharedManager];
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.jpg"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.tableView setBackgroundView:backgroundImageView];
    
    cellInfo = [NSMutableArray arrayWithArray:[CadastreRowInfo listOfItens]];
    [self.navigationController setNavigationBarHidden: NO animated:NO];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (pickerOnScreen) {
        [self dismissPickerView];
    }
    
    activeTextfield = textField;
    NSIndexPath *scrollIndexPath = [self.tableView indexPathForCell:(UITableViewCell *)textField.superview.superview];
    [self.tableView selectRowAtIndexPath:scrollIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [[cellInfo objectAtIndex:textField.tag] setTextOfTextfield:textField.text];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [textField setUserInteractionEnabled:NO];
    
    
    if (textField.returnKeyType == UIReturnKeyNext) {
        [self handleNextButton:activeTextfield];
    }
    
    NSIndexPath *scrollIndexPath = [self.tableView indexPathForCell:(UITableViewCell *)textField.superview.superview];
    if (textField.text.length == 0) {
        UITableViewCell *cell;
        cell = [self.tableView cellForRowAtIndexPath:scrollIndexPath];
    }
    [self.tableView deselectRowAtIndexPath:scrollIndexPath animated:NO];
    
    return NO;
}

- (BOOL)                textField:(UITextField *)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string {
    
    
    if (textField.tag == 2) {
        return [self editingCEPTextfield:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    if (textField.tag == 3 || textField.tag == 4 ) {
        return [self editingStateAndCityTextfield:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    if (textField.tag == 5) {
        return [self editingNumberTextfield:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    if (textField.tag == 6) {
        return [self editingCPFTextfield:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    if (textField.tag == 7) {
        return [self editingEmailTextfield:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return YES;
}

-(BOOL)editingNumberTextfield:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
            replacementString:(NSString *)string{
    if(range.length == 1 && range.location != (textField.text.length - 1))
        return NO;
    else if (range.length == 0 && range.location != textField.text.length)
        return NO;
    
    if(string.length > 1)
        return NO;
    
    // All digits entered
    if (range.location == 13)
        return NO;
    
    if ((textField.text.length + string.length) > 13)
        return NO;
    
    // Reject appending non-digit characters
    if (range.length == 0 &&
        ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]])
        return NO;
    
    // Auto-add hyphen before appending 4rd or 7th digit
    if (range.length == 0 && (range.location == 0)){
        textField.text = [NSString stringWithFormat:@"(%@", string];
        return NO;
    }
    
    if (range.length == 0 && (range.location == 3)){
        textField.text = [NSString stringWithFormat:@"%@)%@",textField.text, string];
        return NO;
    }
    
    if (range.length == 0 && range.location == 8){
        textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
        return NO;
    }
    
    // Delete hyphen when deleting its trailing digit
    if (range.length == 1 && (range.location == 1 || range.location == 4 || range.location == 9)){
        range.location--;
        range.length = 2;
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        return NO;
    }
    
    return YES;
}

-(BOOL)editingEmailTextfield:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
           replacementString:(NSString *)string{
    if (range.length == 0) {
        if (!NSEqualRanges([string rangeOfCharacterFromSet:
                            [NSCharacterSet whitespaceCharacterSet]],
                           NSMakeRange(NSNotFound, 0))) {
            return NO;
        }
    }
    
    return YES;
}

-(BOOL)editingStateAndCityTextfield:(UITextField *)textField
      shouldChangeCharactersInRange:(NSRange)range
                  replacementString:(NSString *)string{
    
    NSCharacterSet *unwantedCharacters =
    [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    
    NSString *trimmedString = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    for (int i = 0; i<trimmedString.length; i++) {
        if([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[trimmedString characterAtIndex:i]])
            return NO;
        
        if(!([trimmedString rangeOfCharacterFromSet:unwantedCharacters].location == NSNotFound))
            return NO;
    }
    
    return YES;
}

-(BOOL)editingCPFTextfield:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
         replacementString:(NSString *)string{
    if(range.length == 1 && range.location != (textField.text.length - 1))
        return NO;
    else if (range.length == 0 && range.location != textField.text.length)
        return NO;
    
    if(string.length > 1)
        return NO;
    
    // All digits entered
    if (range.location == 14)
        return NO;
    
    if ((textField.text.length + string.length) > 14)
        return NO;
    
    // Reject appending non-digit characters
    if (range.length == 0 &&
        ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]])
        return NO;
    
    // Auto-add hyphen before appending 4rd or 7th digit
    if (range.length == 0 && (range.location == 3 || range.location == 7)){
        textField.text = [NSString stringWithFormat:@"%@.%@", textField.text, string];
        return NO;
    }
    
    if (range.length == 0 && range.location == 11){
        textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
        return NO;
    }
    
    // Delete hyphen when deleting its trailing digit
    if (range.length == 1 && (range.location == 4 || range.location == 8 || range.location == 12)){
        range.location--;
        range.length = 2;
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        return NO;
    }
    
    return YES;
}

-(BOOL)editingCEPTextfield:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
         replacementString:(NSString *)string{
    
    if(range.length == 1 && range.location != (textField.text.length - 1))
        return NO;
    else if (range.length == 0 && range.location != textField.text.length)
        return NO;
    
    if(string.length > 1)
        return NO;
    
    // All digits entered
    if (range.location == 10)
        return NO;
    
    if ((textField.text.length + string.length) > 10)
        return NO;
    
    // Reject appending non-digit characters
    if (range.length == 0 &&
        ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]])
        return NO;
    
    // Auto-add hyphen before appending 4rd or 7th digit
    if (range.length == 0 && range.location == 2){
        textField.text = [NSString stringWithFormat:@"%@.%@", textField.text, string];
        return NO;
    }
    
    if (range.length == 0 && range.location == 6){
        textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
        return NO;
    }
    
    // Delete hyphen when deleting its trailing digit
    if (range.length == 1 && (range.location == 3 || range.location == 7)){
        range.location--;
        range.length = 2;
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        return NO;
    }
    
    return YES;
}

- (void)birthdateRowPressed:(NSIndexPath *)indexPath {
    
    if(pickerOnScreen)
        return;
    pickerOnScreen = YES;
    
    [activeTextfield resignFirstResponder];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 215, 320, 215)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = NO;
    datePicker.date = [NSDate date];
    [datePicker addTarget:self
                   action:@selector(LabelChange)
         forControlEvents:UIControlEventValueChanged];
    
    
    CGRect tableFrame = self.tableView.frame;
    tableFrame.size.height -= 255;
    [self.tableView setFrame:tableFrame];
    
    
    ItemRegistrationCell *cell = (ItemRegistrationCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    activeTextfield = cell.textField;
    CGPoint localPoint = [cell bounds].origin;
    CGPoint basePoint = [cell convertPoint:localPoint toView:self.view];
    [self.tableView setContentOffset:CGPointMake(0, basePoint.y - 160)];
    [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.frame = CGRectMake(0, datePicker.frame.origin.y - 40 , self.view.frame.size.width , 40);
    closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Seguinte"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(self.view.frame.size.width - 77, 7.0f, 70.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissPickerViewAndHandleNext) forControlEvents:UIControlEventValueChanged];
    [toolbar addSubview:closeButton];
    
    [self.view.superview addSubview:toolbar];
    [self.view.superview addSubview:datePicker];
}

-(void)dismissPickerView{
    [datePicker removeFromSuperview];
    [toolbar removeFromSuperview];
    [self.tableView setFrame:self.tableView.superview.bounds];
    pickerOnScreen = FALSE;
}

-(void)dismissPickerViewAndHandleNext{
    
    [self dismissPickerView];
    [self handleNextButton:activeTextfield];
    
    
}

-(void)handleNextButton:(UIView *)v{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)v.superview.superview];
    NSIndexPath *nextRowIndexPath = [NSIndexPath indexPathForRow:(indexPath.row+1) inSection:indexPath.section];
    [self.tableView selectRowAtIndexPath:nextRowIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [self tableView:self.tableView didSelectRowAtIndexPath:nextRowIndexPath];
    return;
}

- (void)LabelChange{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    activeTextfield.text = [NSString stringWithFormat:@"%@",
                            [df stringFromDate:datePicker.date]];
    [[cellInfo objectAtIndex:activeTextfield.tag] setTextOfTextfield:activeTextfield.text];
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0)
        return 11;
    else
        return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *simpleTableIdentifier = @"ItemRegistrationCell";
        
        ItemRegistrationCell *cell = (ItemRegistrationCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemRegistrationCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        
        
        cell.titleName.text = [[cellInfo objectAtIndex:indexPath.row] title];
        cell.textField.delegate = self;
        cell.textField.returnKeyType = UIReturnKeyNext;
        cell.textField.text = [[cellInfo objectAtIndex:indexPath.row] textOfTextfield];
        
        if ([[[cellInfo objectAtIndex:indexPath.row] validValue] isEqualToNumber:[NSNumber numberWithBool:FALSE]]) {
            [cell setBackgroundColor:[UIColor colorWithRed:255/255.f green:200/255.f blue:200/255.f alpha:1.0]];
        }
        else{
            [cell setBackgroundColor:[UIColor whiteColor]];
        }
        
        switch (indexPath.row) {
            case 0:
                [cell.textField setTag:0];
                cell.textField.secureTextEntry = NO;
                self.nameTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
            case 1:
                [cell.textField setTag:1];
                cell.textField.secureTextEntry = NO;
                self.adressTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
                
            case 2:
                [cell.textField setTag:2];
                cell.textField.secureTextEntry = NO;
                cell.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                self.CEPTextfield = cell.textField;
                break;
                
            case 3:
                [cell.textField setTag:3];
                cell.textField.secureTextEntry = NO;
                self.cityTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
                
            case 4:
                [cell.textField setTag:4];
                cell.textField.secureTextEntry = NO;
                self.stateTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
                
            case 5:
                [cell.textField setTag:5];
                cell.textField.secureTextEntry = NO;
                cell.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                self.numberTextfield = cell.textField;
                break;
                
            case 6:
                [cell.textField setTag:6];
                cell.textField.secureTextEntry = NO;
                cell.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                self.CPFTextfield = cell.textField;
                break;
                
            case 7:
                [cell.textField setTag:7];
                cell.textField.secureTextEntry = NO;
                cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
                self.emailTextfield = cell.textField;
                break;
                
            case 8:
                [cell.textField setTag:8];
                cell.textField.secureTextEntry = NO;
                self.birthdateTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
                
            case 9:
                [cell.textField setTag:9];
                cell.textField.secureTextEntry = YES;
                self.passwordTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                break;
                
            case 10:
                [cell.textField setTag:10];
                cell.textField.secureTextEntry = YES;
                self.rewritePasswordTextfield = cell.textField;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.textField.returnKeyType = UIReturnKeyDefault;
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    
    else{
        static NSString *simpleTableIdentifier = @"ValidateCadastreCell";
        
        ValidateCadastreCell *cell = (ValidateCadastreCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValidateCadastreCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        return cell;
    }
    
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [self textFieldShouldReturn:activeTextfield];
        [self handleRegister];
        return;
    }
    
    else{
        [[cellInfo objectAtIndex:indexPath.row] setValidValue:[NSNumber numberWithBool:TRUE]];
        [self.tableView reloadData];
        
        for (UIView *subview in [tableView cellForRowAtIndexPath:indexPath].contentView.subviews) {
            if ([subview isKindOfClass:[UITextField class]]) {
                
                if (subview.tag == 8) {
                    [self birthdateRowPressed:(NSIndexPath *)indexPath];
                }
                else{
                    [subview setUserInteractionEnabled:YES];
                    [subview becomeFirstResponder];
                }
            }
        }
    }
}


-(void)handleRegister{
    NSArray *textfieldArray = [NSArray arrayWithObjects:self.nameTextfield, self.adressTextfield, self.CEPTextfield, self.cityTextfield, self.stateTextfield, self.numberTextfield, self.CPFTextfield, self.emailTextfield, self.birthdateTextfield, self.passwordTextfield, self.rewritePasswordTextfield, nil];
    BOOL validRegister = TRUE;
    for (int i = 0;  i < 11; i++) {
        UITextField *textfield = [textfieldArray objectAtIndex:i];
        
        if (textfield.text.length == 0) {
            [[cellInfo objectAtIndex:i] setValidValue:[NSNumber numberWithBool:FALSE]];
            validRegister = FALSE;
        }
        
        else if (textfield.tag == 2 && (textfield.text.length < 10)) {
            [[cellInfo objectAtIndex:i] setValidValue:[NSNumber numberWithBool:FALSE]];
            validRegister = FALSE;
        }
        else if (textfield.tag == 5 &&(textfield.text.length < 13) ){
            [[cellInfo objectAtIndex:i] setValidValue:[NSNumber numberWithBool:FALSE]];
            validRegister = FALSE;
        }
        else if (textfield.tag == 6 &&(textfield.text.length < 14) ){
            [[cellInfo objectAtIndex:i] setValidValue:[NSNumber numberWithBool:FALSE]];
            validRegister = FALSE;
        }
    }
    if (![self.passwordTextfield.text isEqualToString:self.rewritePasswordTextfield.text]) {
        [[cellInfo objectAtIndex:[textfieldArray indexOfObject:self.passwordTextfield]] setValidValue:[NSNumber numberWithBool:FALSE]];
        [[cellInfo objectAtIndex:[textfieldArray indexOfObject:self.rewritePasswordTextfield]] setValidValue:[NSNumber numberWithBool:FALSE]];
        validRegister = FALSE;
    }
    
    if (!validRegister) {
        [self.tableView reloadData];
        return;
    }
    
    [self registerUser];
}

-(void)registerUser{

    Usuario *user = [[Usuario alloc] init];
    
    user.email = self.emailTextfield.text;
    user.adress = self.adressTextfield.text;
    NSString *phoneWithoutLeftBrace = [self.numberTextfield.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *phoneWithoutRightBrace = [phoneWithoutLeftBrace stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *phoneNumeric = [phoneWithoutRightBrace stringByReplacingOccurrencesOfString:@"-" withString:@""];
    user.phone = phoneNumeric;
    user.name = self.nameTextfield.text;
    NSString *CEPWithoutDash = [self.CEPTextfield.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *CEPNumeric = [CEPWithoutDash stringByReplacingOccurrencesOfString:@"." withString:@""];
    user.cep = CEPNumeric;
    user.rg = nil;
    NSString *CPFWithoutDash = [self.CPFTextfield.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *CPFNumeric = [CPFWithoutDash stringByReplacingOccurrencesOfString:@"." withString:@""];
    user.cpf = CPFNumeric;
    
    user.city = self.cityTextfield.text;
    user.state = self.stateTextfield.text;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSDate *myDate = [df dateFromString: self.birthdateTextfield.text];
    NSLog(@"%@", myDate);
    user.birthdate = myDate;
    user.password = self.passwordTextfield.text;
    
    [objectManager postObject:user delegate:self];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Usuário criado com sucesso!"
                                                        message:@"Você agora pode acessar o Ticket Mobile utilizando o e-mail e senha escolhidos!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
    alertView.tag = 1;
    alertView.delegate = self;
    [alertView show];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    
    if ([[objectLoader response] statusCode] == 422) {

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erro de autenticação."
                                                        message:@"Você já cadastrou usuário usando estes dados anteriormente? Foi encontrado dados duplicados e não foi possível autenticar novo usuário, verifique as informações e tente novamente."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        alertView.tag = 2;
        alertView.delegate = self;
        [alertView show];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Problema de conexão."
                                                            message:@"Seu usuário não pode ser criado por problemas de conexão ao servidor. Certifique-se de estar conectado a internet e tente novamente."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        alertView.tag = 2;
        alertView.delegate = self;
        [alertView show];
    }
}

#pragma mark RKRequestDelegate methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
            // Success! Let's take a look at the data
            NSLog(@"Retrieved XML: %@", [response bodyAsString]);
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            NSLog(@"JSON Response from Server %@", [response bodyAsString] );
            
        }
        
    } else if ([request isDELETE]) {
        
        // Handling DELETE /missing_resource.txt
        if ([response isNotFound]) {
            NSLog(@"The resource path '%@' was not found.", [request resourcePath]);
        }
    }
}

- (void)request:(RKRequest *)request didFailAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
}

@end