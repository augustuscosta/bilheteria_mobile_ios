//
//  LegalViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalViewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
