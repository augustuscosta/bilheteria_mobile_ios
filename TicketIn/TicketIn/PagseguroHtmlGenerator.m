//
//  PagseguroHtmlGenerator.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 24/04/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "PagseguroHtmlGenerator.h"

@implementation PagseguroHtmlGenerator

-(NSString *)returnHtml:(NSArray *)itensId iDescription:(NSArray*)itensDescription iAmount:(NSArray *)itensAmount{
    
    NSString *firstObrigatoryItemsString = [NSString stringWithFormat:
                                            @"<form target='pagseguro' method='post'action='https://pagseguro.uol.com.br/v2/checkout/payment.html'>\
                                            <input type='hidden' name='receiverEmail' value='helio@otgmobile.com.br'>\
                                            <input type='hidden' name='currency' value='BRL'>"];
    NSString *finalString = [NSString stringWithFormat:@"%@", firstObrigatoryItemsString];
    
    for (int i = 0; i < itensId.count; i++) {
        NSString *itemString = [self baseItems:(i+1) itemId:[itensId objectAtIndex:i] itemDescription:[itensDescription objectAtIndex:i] itemAmount:[itensAmount objectAtIndex:i]];
        NSString *string = [NSString stringWithFormat:@"%@%@", finalString, itemString ];
        finalString = string;
    }
    
    finalString = [finalString stringByAppendingString:@"<table cellpadding=\"0\" cellspacing=\"0\"\
                                                        border=\"0\" height=\"100%\" width=\"100%\">\
                                                        <tr><td align=\"center\" valign=\"middle\">\
                                                        <input type='image' name='submit'\
                                                        src='https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar.gif'\
                                                        alt='Pague com PagSeguro'>\
                                                        </td></tr></table></form>"];
    
    return finalString;
}

-(NSString*)baseItems:(int)numberId itemId:(NSString*)iId itemDescription:(NSString *)iDescription itemAmount:(NSString*)iAmount {
    NSString* itemIdString = [NSString stringWithFormat:@"<input type='hidden' name='itemId%d' value='%@'>  ", numberId, iId];
    NSString* itemDescriptionString = [NSString stringWithFormat:@"<input type='hidden' name='itemDescription%d' value='%@'> ", numberId, iDescription];
    NSString* itemAmountString = [NSString stringWithFormat:@"<input type='hidden' name='itemAmount%d' value='%@'> ", numberId, iAmount];
    NSString* itemQuantityString = [NSString stringWithFormat:@"<input type='hidden' name='itemQuantity%d' value='1'> ", numberId];
    
    NSString* returnString = [NSString stringWithFormat:@"%@%@%@%@", itemIdString, itemDescriptionString, itemAmountString, itemQuantityString];
    return returnString;
}

@end
