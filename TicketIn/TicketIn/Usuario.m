//
//  Usuario.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 06/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Usuario.h"


@implementation Usuario

@synthesize email;
@synthesize adress;
@synthesize phone;
@synthesize name;
@synthesize cep;
@synthesize rg;
@synthesize cpf;
@synthesize city;
@synthesize state;
@synthesize birthdate;
@synthesize password;

@end
