//
//  Ingresso.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 19/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Ingresso.h"


@implementation Ingresso

@dynamic conteudoId;
@dynamic resposta;
@dynamic tipoDeIngressoId;
@dynamic transacao;
@dynamic usuarioId;
@dynamic ingressoId;
@dynamic createdAt;

@end
