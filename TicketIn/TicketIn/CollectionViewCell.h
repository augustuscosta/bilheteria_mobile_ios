//
//  CollectionViewCell.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *collectionViewImage;

@end
