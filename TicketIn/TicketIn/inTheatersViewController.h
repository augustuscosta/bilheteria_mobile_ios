//
//  inTheatersViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 03/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"

@protocol InTheatersViewControllerDelegate <NSObject>

-(void)handleSelectRowOnInTheatersEvents:(Conteudo *)conteudo;

@end

@interface InTheatersViewController : UITableViewController{
    id <InTheatersViewControllerDelegate>delegate;
}

@property (nonatomic, retain) id <InTheatersViewControllerDelegate>delegate;

@property (strong, nonatomic) NSMutableArray* allTableData;
@property (strong, nonatomic) NSMutableArray* filteredTableData;

@property (nonatomic, retain) NSMutableArray *inTheatersConteudos;
@property (nonatomic, assign) bool isFiltered;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
