//
//  MapPoint.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 18/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Conteudo.h"

@interface MapPoint : NSObject<MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) Conteudo *conteudo;

-(id) initWithCoordinate: (CLLocationCoordinate2D) c title:(NSString *) t subtitle: (NSString *) sub conteudo: (Conteudo *) ct;

@end
