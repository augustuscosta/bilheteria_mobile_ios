//
//  Empresa.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Empresa : NSManagedObject

@property (nonatomic, retain) NSString * cnpj;
@property (nonatomic, retain) NSDate * createAt;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * empresaId;
@property (nonatomic, retain) NSString * identificar;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * telefone;
@property (nonatomic, retain) NSDate * updatedAt;

@end
