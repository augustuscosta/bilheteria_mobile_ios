//
//  TicketCell.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 24/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"

@interface TicketCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *showName;
@property (strong, nonatomic) IBOutlet UILabel *showType;
@property (strong, nonatomic) IBOutlet UILabel *showDate;
@property (strong, nonatomic) IBOutlet UILabel *showHour;
@property (strong, nonatomic) NSString *QRCode;
@property (strong, nonatomic) Conteudo *conteudo;
@property (strong, nonatomic) IBOutlet UIButton *showInfo;
@property (strong, nonatomic) IBOutlet UIImageView *previewQRCode;

@end
