//
//  Session.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 05/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface Session : NSObject

@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* password;

@end
