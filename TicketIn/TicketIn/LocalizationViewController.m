//
//  LocalizationViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 15/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "LocalizationViewController.h"
#import "MapPoint.h"

@interface LocalizationViewController ()

@end

@implementation LocalizationViewController

@synthesize conteudo = _conteudo;
@synthesize mapView = _mapView;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setHidden:NO];
    self.title = self.conteudo.titulo;
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setPinLocation:self.mapView];
}

-(void)setPinLocation : (MKMapView *)mapView{
    NSArray *ants = [mapView annotations];
    [mapView removeAnnotations:ants];
    
    CGFloat latDelta = [self.conteudo.latitude floatValue];
    CGFloat longDelta = [self.conteudo.longitude floatValue];
    CLLocationCoordinate2D newCoordinate = {latDelta, longDelta};
    MapPoint *mapPoint = [[MapPoint alloc] initWithCoordinate:newCoordinate title:self.conteudo.titulo subtitle:self.conteudo.subtitulo conteudo:self.conteudo];
    
    [mapView addAnnotation:mapPoint];
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }
    [mapView setVisibleMapRect:zoomRect animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    NSString *annotationIdentifier = @"PinViewAnnotation";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mv dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!pinView)
    {
        pinView = [[MKPinAnnotationView alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        [pinView setPinColor:MKPinAnnotationColorGreen];
        pinView.canShowCallout = YES;
        
    }
    else
        pinView.annotation = annotation;
    
    
    return pinView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
