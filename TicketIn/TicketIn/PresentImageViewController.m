//
//  PresentImageViewController.m
//  skynet-version
//
//  Created by Nichollas Fonseca on 26/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "PresentImageViewController.h"
#import "EventImageView.h"

@interface PresentImageViewController ()

@end

@implementation PresentImageViewController

@synthesize imageView = _imageView;
@synthesize imageToLoad = _imageToLoad;
@synthesize toolbar = _toolbar;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.conteudo.titulo;
    [[self.navigationController navigationBar] setBarStyle:UIBarStyleBlackTranslucent];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self downloadAndLoadImage];
}

-(void)downloadAndLoadImage{
    [self startViews];
}

-(void)startViews{
    
    [self createScrollView:[self createImageView]];
    [self.view addSubview:self.scrollView];
    if ([self.conteudo.social isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        [self createToolbar];
        [self.toolbar setItems:[self createToolbarItems]];
    }
}

-(void)createScrollView:(UIImageView *)withImageView{
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView setMaximumZoomScale:2.0f];
    [self.scrollView setMinimumZoomScale:1.0f];
    self.scrollView.contentSize = self.view.bounds.size;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setDelegate:self];
    [self.scrollView addSubview:withImageView];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self action:@selector(handleDoubleTap)];
    doubleTapGestureRecognizer.numberOfTapsRequired = 2;
    doubleTapGestureRecognizer.delegate = self;
    [self.scrollView addGestureRecognizer:doubleTapGestureRecognizer];

    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    
    [singleTapGestureRecognizer requireGestureRecognizerToFail: doubleTapGestureRecognizer];
    singleTapGestureRecognizer.delegate = self;
    [self.scrollView addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)handleDoubleTap{
    if(self.scrollView.zoomScale > self.scrollView.minimumZoomScale)
        [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
    else
        [self.scrollView setZoomScale:self.scrollView.maximumZoomScale animated:YES];
}

-(void)handleSingleTap{
    if (self.navigationController.navigationBarHidden == YES){
        //[[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.navigationController setNavigationBarHidden: NO animated:YES];
        [self.toolbar setHidden:NO];
    }
    else{
        //[[UIApplication sharedApplication] setStatusBarHidden:YES];
        [self.navigationController setNavigationBarHidden: YES animated:YES];
        [self.toolbar setHidden:YES];
    }
}

-(UIImageView *)createImageView{
    self.imageView = [[EventImageView alloc] initWithFrame:self.view.bounds url:[NSURL URLWithString:self.conteudo.imagemUrl]];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.userInteractionEnabled = YES;
    return self.imageView;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark toolbar and social methods

-(void)createToolbar{
    self.toolbar = [[UIToolbar alloc]init];
    self.toolbar.frame = CGRectMake(0, self.view.bounds.size.height - 44, self.view.bounds.size.width, 44);
    
    self.toolbar.barStyle = UIBarStyleBlackTranslucent;
    [self.view addSubview:self.toolbar];
}

-(NSMutableArray *)createToolbarItems{
    UIImage *facebookImage = [UIImage imageNamed:@"fb-share1.png"];
    UIImage *twitterImage = [UIImage imageNamed:@"tt-share1.png"];
    itensSizeWidth = facebookImage.size.width;
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    facebookButton = [[UIBarButtonItem alloc] initWithImage:facebookImage style:UIBarButtonItemStylePlain target:self action:@selector(handleShareFacebook)];
	twitterButton = [[UIBarButtonItem alloc] initWithImage:twitterImage style:UIBarButtonItemStylePlain target:self action:@selector(handleShareTwitter)];
    
    facebookButton.width = self.toolbar.frame.size.width/2 - itensSizeWidth * 0.5;
    twitterButton.width = self.toolbar.frame.size.width/2 - itensSizeWidth * 0.5;
    
    [barItems insertObject:facebookButton atIndex:0];
    [barItems insertObject:twitterButton atIndex:0];
    
    return barItems;
}

-(void)handleShareFacebook{
    NSString *message = [self.conteudo descricao];
    UIImage *image = self.imageView.image;
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText:message];
        [facebookSheet addImage:image];
        [self presentViewController:facebookSheet animated:YES completion:nil];
        
    }
}

-(void)handleShareTwitter{
    NSString *message = [self.conteudo descricao];
    UIImage *image = self.imageView.image;
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:message];
        [tweetSheet addImage:image];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
    }
}

@end
