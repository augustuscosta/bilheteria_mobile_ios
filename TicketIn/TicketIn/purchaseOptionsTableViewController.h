//
//  purchaseOptionsTableViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 17/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

@interface purchaseOptionsTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate, RKObjectLoaderDelegate>{
    UIPickerView *pickerView;
    UIToolbar *toolbar;
    UISegmentedControl *closeButton;
    BOOL pickerOnScreen;
    RKObjectManager* objectManager;
    NSInteger numberOfTicketsInQueue;
    
    NSMutableArray *searchNumberOfTyckets;
    
    NSInteger numberOfTicketsSelectedIndex;
    NSInteger typeOfTicketSelectedIndex;
}

@property (nonatomic, retain)Conteudo *conteudo;
@property (nonatomic, retain)NSMutableArray *tiposDeIngressos;

@end
