//
//  TesteView.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 05/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventImageView.h"

@implementation EventImageView

@synthesize url = _url;
@synthesize index = _index;

-(id)init{
    self = [super init];
    if (self) {
        
        //[HUD showWhileExecuting:@selector(myTask:) onTarget:self withObject:self.url animated:YES];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame url:(NSURL *)url
{
    self = [super initWithFrame:frame];
    if (self) {
        self.url = url;
        HUD = [[MBProgressHUD alloc] initWithView:self];
        [self addSubview:HUD];
        
        [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
        
    }
    return self;
}


-(void)myTask{
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:self.url];
    UIImage *imageToReturn = [UIImage imageWithData:imageData];
    [self setImage:imageToReturn];
}

@end
