//
//  TesteView.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 05/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface EventImageView : UIImageView{
    MBProgressHUD *HUD;
}

@property (nonatomic, retain) NSURL *url;
@property float index;

-(id)initWithFrame:(CGRect)frame url:(NSURL *)url;

@end
