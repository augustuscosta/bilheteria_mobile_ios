//
//  CadastreViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface CadastreViewController : UITableViewController<UITextFieldDelegate, RKRequestDelegate, RKObjectLoaderDelegate, UIAlertViewDelegate>{
    BOOL pickerOnScreen;
    UIDatePicker *datePicker;
    UIToolbar *toolbar;
    UISegmentedControl *closeButton;
    NSArray *textfieldsArray;
    UITextField *activeTextfield;
    BOOL alreadyHasDot;
    NSMutableArray *cellInfo;
    RKObjectManager* objectManager;
}
@property (strong, nonatomic)  UITextField *nameTextfield;
@property (strong, nonatomic)  UITextField *adressTextfield;
@property (strong, nonatomic)  UITextField *CEPTextfield;
@property (strong, nonatomic)  UITextField *stateTextfield;
@property (strong, nonatomic)  UITextField *cityTextfield;
@property (strong, nonatomic)  UITextField *numberTextfield;
@property (strong, nonatomic)  UITextField *CPFTextfield;
@property (strong, nonatomic)  UITextField *emailTextfield;
@property (strong, nonatomic)  UIButton *birthdateButton;
@property (strong, nonatomic)  UITextField *passwordTextfield;
@property (strong, nonatomic)  UITextField *rewritePasswordTextfield;
@property (strong, nonatomic)  UITextField *birthdateTextfield;
@property (strong, nonatomic) UIImage *backgroundImageOriginal;
@property (strong, nonatomic) UIImage *backgroundImageResized;


@end
