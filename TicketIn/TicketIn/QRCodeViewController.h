//
//  QRCodeViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 15/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface QRCodeViewController : UIViewController<MFMailComposeViewControllerDelegate>{
    UIImage *QRCodeImage;
}

@property(nonatomic, retain)NSString *stringToGenerateQRCode;
@property(nonatomic, retain)Conteudo *conteudo;

-(id)initWithStringAndConteudo:(NSString *)string conteudo:(Conteudo *)conteudo;

@end
