//
//  CartViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 26/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "PaymentWebViewController.h"

typedef enum PaymentStatuses {
	PAYMENTSTATUS_SUCCESS,
	PAYMENTSTATUS_FAILED,
	PAYMENTSTATUS_CANCELED,
} PaymentStatus;

@protocol CartViewControllerDelegate <NSObject>

-(void)handleOpenTAU;
-(void)handleOpenPayment:(NSArray *)products;

@end

@interface CartViewController : UITableViewController<RKRequestDelegate, RKObjectLoaderDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIWebViewDelegate, PaymentWebViewDelegate>{
    RKObjectManager *objectManager;
    NSInteger numberOfTickets;
    PaymentStatus paymentStatus;
    NSString *paypalCode;
    id <CartViewControllerDelegate>delegate;
    NSMutableArray *products;
}


@property (strong, nonatomic)NSMutableArray *tickets;
@property (nonatomic, retain)NSMutableArray *conteudos;
@property (nonatomic, retain)UIWebView *webView;
@property (nonatomic, retain)NSNumber *subtotalValue;
@property (nonatomic, retain)NSNumber *totalValue;
@property (nonatomic, retain)NSNumber *serviceChargeValue;
@property (nonatomic, retain) id <CartViewControllerDelegate>delegate;

@end
