//
//  CheckoutCell.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 03/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckoutCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *subtotalValue;
@property (strong, nonatomic) IBOutlet UILabel *chargeValue;
@property (strong, nonatomic) IBOutlet UILabel *totalValue;
@property (strong, nonatomic) IBOutlet UIButton *paypalButton;

@end
