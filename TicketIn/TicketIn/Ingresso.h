//
//  Ingresso.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 19/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Ingresso : NSManagedObject

@property (nonatomic, retain) NSNumber * conteudoId;
@property (nonatomic, retain) NSString * resposta;
@property (nonatomic, retain) NSNumber * tipoDeIngressoId;
@property (nonatomic, retain) NSString * transacao;
@property (nonatomic, retain) NSNumber * usuarioId;
@property (nonatomic, retain) NSNumber * ingressoId;
@property (nonatomic, retain) NSDate * createdAt;

@end
