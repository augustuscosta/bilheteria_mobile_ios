//
//  ContainerViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 14/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeaturedViewController.h"
#import "InTheatersViewController.h"
#import "SoonViewController.h"
#import "CartViewController.h"
#import <RestKit/RestKit.h>
#import "Conteudo.h"
#import "purchaseOptionsTableViewController.h"
#import "ListOfTicketsViewController.h"
#import "MBProgressHUD.h"

@interface ContainerViewController : UIViewController<RKRequestDelegate, RKObjectLoaderDelegate, FeaturedViewControllerDelegate, SoonViewControllerDelegate, InTheatersViewControllerDelegate, CartViewControllerDelegate, UIAlertViewDelegate>{
    BOOL loadingHome;
    FeaturedViewController *fVC;
    InTheatersViewController *iTVC;
    SoonViewController *sVC;
    CartViewController *cVC;
    ListOfTicketsViewController *lTVC;
    Conteudo *featuredConteudo;
    BOOL ticketsViewLoaded;
    BOOL soonViewLoaded;
    BOOL inTheatersViewLoaded;
    BOOL cartViewLoaded;
    RKObjectManager* objectManager;
    id vcInScene;
    MBProgressHUD *HUD;
    UIButton *logoutButton;
}

@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) IBOutlet UIButton *featuredButton;
@property (strong, nonatomic) IBOutlet UIButton *inTheatersButton;
@property (strong, nonatomic) IBOutlet UIButton *soonButton;
@property (strong, nonatomic) IBOutlet UIButton *ticketsButton;
@property (strong, nonatomic) IBOutlet UIButton *cartButton;
@property (nonatomic, retain) NSMutableArray *allConteudos;
-(void)handleShowCart;

@end
