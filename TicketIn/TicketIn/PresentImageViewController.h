//
//  PresentImageViewController.h
//  skynet-version
//
//  Created by Nichollas Fonseca on 26/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import "Twitter/Twitter.h"
#import "Social/Social.h"
#import "EventImageView.h"

@interface PresentImageViewController : UIViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>{
    float itensSizeWidth;
    UIBarButtonItem *facebookButton;
	UIBarButtonItem *twitterButton;
}

@property (nonatomic, retain)UIImageView *imageView;
@property (nonatomic, retain)UIImage *imageToLoad;
@property (nonatomic, retain)UIScrollView *scrollView;
@property (nonatomic, retain)Conteudo *conteudo;
@property (nonatomic,retain)UIToolbar *toolbar;

@end
