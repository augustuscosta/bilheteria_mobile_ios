//
//  LegalViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 04/01/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "LegalViewController.h"
#import "QREncoder.h"

@interface LegalViewController ()

@end

@implementation LegalViewController

@synthesize webView = _webView;
@synthesize scrollView = _scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Termos e Usos";
    
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                                          pathForResource:@"termos" ofType:@"html"]isDirectory:NO]]];
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
