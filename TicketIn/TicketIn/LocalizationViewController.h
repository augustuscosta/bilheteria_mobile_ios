//
//  LocalizationViewController.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 15/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conteudo.h"
#import <MapKit/MapKit.h>

@interface LocalizationViewController : UIViewController

@property (nonatomic, retain)Conteudo *conteudo;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end
