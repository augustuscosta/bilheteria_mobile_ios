//
//  QRCodeViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 15/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "QRCodeViewController.h"
#import "QREncoder.h"

@interface QRCodeViewController ()

@end

@implementation QRCodeViewController

@synthesize stringToGenerateQRCode = _stringToGenerateQRCode;
@synthesize conteudo = _conteudo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithStringAndConteudo:(NSString *)string conteudo:(Conteudo *)conteudo{
    self = [super init];
    if (self) {
        self.stringToGenerateQRCode = string;
        self.conteudo = conteudo;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:NO];
    self.title = self.conteudo.titulo;
    UIImageView* qrcodeImageView;
    
    if ([self.stringToGenerateQRCode isEqualToString:@""]) {
        UIImage *processingImage = [UIImage imageNamed:@"processing.jpg"];
        qrcodeImageView = [[UIImageView alloc] initWithImage:processingImage];
        [qrcodeImageView setFrame:CGRectMake(self.view.bounds.size.width/2 - processingImage.size.width/2, self.view.bounds.size.height/2 - processingImage.size.height/2 - 40, processingImage.size.width, processingImage.size.height)];
    }
    else{
        // Do any additional setup after loading the view.
        
        //the qrcode is square. now we make it 250 pixels wide
        int qrcodeImageDimension = 300;
        
        //the string can be very long
        
        //first encode the string into a matrix of bools, TRUE for black dot and FALSE for white. Let the encoder decide the error correction level and version
        DataMatrix* qrMatrix = [QREncoder encodeWithECLevel:QR_ECLEVEL_AUTO version:QR_VERSION_AUTO string:self.stringToGenerateQRCode];
        
        //then render the matrix
        QRCodeImage = [QREncoder renderDataMatrix:qrMatrix imageDimension:qrcodeImageDimension];
        
        //put the image into the view
        qrcodeImageView = [[UIImageView alloc] initWithImage:QRCodeImage];
        CGRect parentFrame = self.view.frame;
        CGRect tabBarFrame = self.tabBarController.tabBar.frame;
        
        //center the image
        CGFloat x = (parentFrame.size.width - qrcodeImageDimension) / 2.0;
        CGFloat y = ((parentFrame.size.height - qrcodeImageDimension - tabBarFrame.size.height) / 2.0) - 40;
        CGRect qrcodeImageViewFrame = CGRectMake(x, y, qrcodeImageDimension, qrcodeImageDimension);
        [qrcodeImageView setFrame:qrcodeImageViewFrame];
    }
    
    //and that's it!
    [self.view addSubview:qrcodeImageView];
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    UIToolbar *toolbar = [[UIToolbar alloc]init];
    toolbar.frame = CGRectMake(0, self.view.frame.size.height - 44, self.view.frame.size.width, 44);
    
    NSLog(@"%f", toolbar.frame.origin.y);
    
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    UIImage *emailImage = [UIImage imageNamed:@"email.png"];
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
	UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithImage:emailImage style:UIBarButtonItemStylePlain target:self action:@selector(handleSendEmail)];
    emailButton.width = toolbar.frame.size.width - emailImage.size.width/2;
    
    [barItems insertObject:emailButton atIndex:0];
    [toolbar setItems:barItems];
    [self.view addSubview:toolbar];
}

-(void)handleSendEmail{
    
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:[NSString stringWithFormat:@"QRCode do show %@", self.conteudo.titulo]];
        
        // Attach an image to the email
        NSData *myData = UIImagePNGRepresentation(QRCodeImage);
        [picker addAttachmentData:myData mimeType:@"image/png" fileName:@"QRCode.png"];
        
        // Fill out the email body text
        NSString *emailBody =[NSString stringWithFormat:@"Cópia de segurança do QRCode para o show: %@", self.conteudo.titulo];
        [picker setMessageBody:emailBody isHTML:NO];
        [self presentViewController:picker animated:YES completion:nil];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: failed");
            break;
        default:
            NSLog(@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
