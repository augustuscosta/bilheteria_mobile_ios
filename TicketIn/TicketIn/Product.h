//
//  Product.h
//  TicketIn
//
//  Created by Nichollas Fonseca on 25/06/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, retain) NSNumber *productID;
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSNumber *productValue;


@end
