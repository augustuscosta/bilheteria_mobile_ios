//
//  PaymentWebViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 25/06/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "PaymentWebViewController.h"
#import "PagseguroHtmlGenerator.h"
#import "Product.h"
#import "HUD.h"

@interface PaymentWebViewController ()

@end

@implementation PaymentWebViewController

@synthesize delegate = _delegate;
@synthesize webView = _webView;
@synthesize products = _products;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firtTimeWeb = TRUE;
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
	// Do any additional setup after loading the view.
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    PagseguroHtmlGenerator *pshg = [[PagseguroHtmlGenerator alloc] init];
    NSMutableArray *idForItens = [[NSMutableArray alloc] init];
    NSMutableArray *descriptionForItens = [[NSMutableArray alloc] init];
    NSMutableArray *amountForItens = [[NSMutableArray alloc] init];
    
    pagSeguroBanner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"funciona_pagseguro.jpg"]];
    [pagSeguroBanner setFrame:CGRectMake(0, 0, 320, 149)];
    [self.view addSubview:pagSeguroBanner];
    
    float serviceCharge = 0.0;
    
    for (int i = 0; i < self.products.count; i++) {
        [idForItens addObject:[NSString stringWithFormat:@"%@",[[self.products objectAtIndex:i] productID]] ];
        [descriptionForItens addObject:[NSString stringWithFormat:@"%@", [[self.products objectAtIndex:i] productName]]];
        [amountForItens addObject:[NSString stringWithFormat:@"%0.2f", [[[self.products objectAtIndex:i] productValue] floatValue]]];
        serviceCharge += 0.15 * [[[self.products objectAtIndex:i] productValue] floatValue];
    }
    
    
    //Adicionar a taxa de serviço.
    [idForItens addObject:[NSString stringWithFormat:@"TAXADESERVICO"]];
    [descriptionForItens addObject:[NSString stringWithFormat:@"Inclusao do valor da taxa de servico."]];
    [amountForItens addObject:[NSString stringWithFormat:@"%0.2f", serviceCharge]];
    [self.webView loadHTMLString:[pshg returnHtml:idForItens iDescription:descriptionForItens iAmount:amountForItens] baseURL:nil];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    //CAPTURE USER LINK-CLICK.
    NSURL *pureURL = [request URL]; 
    NSString *url = [NSString stringWithFormat:@"%@", pureURL];
    if ([url rangeOfString:@"otgmobile"].location == NSNotFound) return YES;
    else {
        if ([self.delegate respondsToSelector:@selector(handleTicketsPayment:)]) {
            NSRange transactionIdRange = [url rangeOfString:@"?transaction_id="];
            NSString *transactionId = [[url substringFromIndex:NSMaxRange(transactionIdRange)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [self.delegate handleTicketsPayment:transactionId];
        }
        [webView stopLoading];
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [HUD showUIBlockingIndicatorWithText:@"Carregando página..."];
}

-(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end fromString:(NSString *)string {
    NSRange startRange = [string rangeOfString:start];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [string length] - targetRange.location;
        NSRange endRange = [string rangeOfString:end options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            return [string substringWithRange:targetRange];
        }
    }
    return nil;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    if (firtTimeWeb)    firtTimeWeb = FALSE;
    else    [pagSeguroBanner removeFromSuperview];
    [HUD hideUIBlockingIndicator];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
