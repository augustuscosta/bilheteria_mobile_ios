//
//  CartViewController.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 26/12/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CartViewController.h"
#import "CartCell.h"
#import "Ingresso.h"
#import "Conteudo.h"
#import "TiposDeIngressos.h"
#import "CheckoutCell.h"
#import "LegalViewController.h"
#import "PagseguroHtmlGenerator.h"
#import "Product.h"
#import "HUD.h"

#define SERVICE_CHARGE 0.15

@interface CartViewController ()

@end

@implementation CartViewController

@synthesize delegate = _delegate;
@synthesize tickets = _tickets;
@synthesize conteudos = _conteudos;
@synthesize subtotalValue = _subtotalValue;
@synthesize totalValue = _totalValue;
@synthesize serviceChargeValue = _serviceChargeValue;
@synthesize webView = _webview;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    products = [[NSMutableArray alloc] init];
    [self.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height - 61)];
    UIImage *originalImage = [UIImage imageNamed:@"bg.jpg"];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[self imageWithImage:originalImage convertToSize:self.view.bounds.size]];
    [self.tableView setBackgroundView:backgroundView];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor colorWithRed:0 green:0.3 blue:0 alpha:1.0];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    objectManager = [RKObjectManager sharedManager];
    numberOfTickets = 0;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [HUD showUIBlockingIndicatorWithText:@"Carregando carrinho..."];
    [self refreshTable];
}

-(void)refreshTable{
    [objectManager loadObjectsAtResourcePath:@"/ingressos" delegate:self];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return numberOfTickets; 
    }
    else if(self.tickets.count > 0)
        return 1;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1)
        return 45;
    else if(indexPath.section == 2)
        return 130;
    else
        return 75;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *simpleTableIdentifier = @"Cell";
        
        //tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divisor.png"]];
        
        CartCell *cell = (CartCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CartCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            UIImage *separatorImage = [UIImage imageNamed:@"divisor.png"];
            UIImageView *separatorImageView = [[UIImageView alloc] initWithImage:separatorImage];
            [separatorImageView setFrame:CGRectMake(0, cell.frame.size.height, separatorImage.size.width, separatorImage.size.height)];
            [cell addSubview:separatorImageView];
        }
        // Configure the cell...
        Conteudo *conteudo;
        for (Conteudo *cont in self.conteudos) {
            if ([cont.conteudoId isEqualToNumber:[[self.tickets objectAtIndex:indexPath.row] conteudoId]])
                conteudo = cont;
        }
        TiposDeIngressos *tDI;
        for (TiposDeIngressos *t in conteudo.tiposDeIngressos) {
            if ([t.tiposDeIngressosId isEqualToNumber:[[self.tickets objectAtIndex:indexPath.row] tipoDeIngressoId]]) {
                tDI = t;
            }
        }
        
        Product *product = [[Product alloc] init];
        product.productID = [[self.tickets objectAtIndex:indexPath.row] ingressoId];
        product.productName = conteudo.titulo;
        product.productValue = tDI.valor;
        [products addObject:product];
        
        cell.ShowName.text = conteudo.titulo;
        
        NSCalendar *calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
        NSDate *date = conteudo.data;
        NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date];
        
        NSInteger year = [dateComponents year];
        NSInteger month = [dateComponents month];
        NSInteger day = [dateComponents day];
        NSInteger hour = [dateComponents hour];
        NSInteger min = [dateComponents minute];
        
        NSString *showTime = [NSString stringWithFormat:@"%02d:%02d", hour, min];
        NSString *showDate = [NSString stringWithFormat:@"%02d.%02d.%d", day, month, year];
        cell.ShowDate.text = showDate;
        cell.ShowTime.text = showTime;
        cell.TicketType.text = tDI.titulo;
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
        NSString *tickerValue = [formatter stringFromNumber:tDI.valor];
        
        cell.TicketValue.text = tickerValue;
        
        return cell;
    }
    if (indexPath.section == 1 && (self.tickets.count > 0)) {
        static NSString *CellIdentifier = @"CellA";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImage *backgroundImage = [UIImage imageNamed:@"barralegal.png"];
            //UIImageView *backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
            //[backgroundView setFrame:CGRectMake(0, 0, cell.frame.size.width, 45)];
            cell.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
            
            UIImage *separatorImage = [UIImage imageNamed:@"divisor.png"];
            UIImageView *separatorTop = [[UIImageView alloc] initWithImage:separatorImage];
            [separatorTop setFrame:CGRectMake(0, 0, separatorImage.size.width, separatorImage.size.height)];
            UIImageView *separatorBottom = [[UIImageView alloc] initWithImage:separatorImage];
            [separatorBottom setFrame:CGRectMake(0, 45, separatorImage.size.width, separatorImage.size.height)];
            [cell addSubview:separatorTop];
            [cell addSubview:separatorBottom];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            UILabel *legalTitle = [[UILabel alloc] init];
            [legalTitle setFont:[UIFont boldSystemFontOfSize:18]];
            legalTitle.text = @"Termos de Uso";
            [legalTitle setFrame:CGRectMake(5,  0, [legalTitle.text sizeWithFont:[UIFont boldSystemFontOfSize:18.0]].width, 45)];
            legalTitle.backgroundColor = [UIColor clearColor];
            [cell addSubview:legalTitle];
            
        }
        
        
        return cell;
    }
    if (indexPath.section == 2 && (self.tickets.count > 0)) {
        static NSString *CellIdentifier = @"CheckoutCell";
        
        CheckoutCell *cell = (CheckoutCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CheckoutCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            UIImage *backgroundImage = [UIImage imageNamed:@"barra.png"];
            cell.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
        }
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
        
        NSString *subtotalString = [formatter stringFromNumber:self.subtotalValue];
        //Calculando a taxa de serviço a 12%.
        NSString *serviceChargeString = [formatter stringFromNumber:self.serviceChargeValue];
        NSString *totalString = [formatter stringFromNumber:self.totalValue];
        UIImage *pagseguroImage = [UIImage imageNamed:@"comprar.png"];
        UIButton *pagseguroButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [pagseguroButton setFrame:CGRectMake(cell.frame.size.width/2 + 5, cell.frame.size.height/2 + 15, pagseguroImage.size.width/2, pagseguroImage.size.height/2)];
        [pagseguroButton setImage:pagseguroImage forState:UIControlStateNormal];
        [pagseguroButton addTarget:self action:@selector(pagseguroButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:pagseguroButton];
        
        [cell addSubview:pagseguroButton];
        
        cell.subtotalValue.text = subtotalString;
        cell.chargeValue.text = serviceChargeString;
        cell.totalValue.text = totalString;
        
        return cell;
    }
    return nil;
}

-(BOOL) isValidIngresso:(Ingresso *)ingresso {
    for(Conteudo* conteudo in self.conteudos){
        if ([ingresso.conteudoId isEqualToNumber:conteudo.conteudoId]) {
            NSDate *fim = conteudo.dataFimVendas;
            NSDate *deviceCurrentDate = [NSDate date];
            NSComparisonResult comparisonFinal = [deviceCurrentDate compare:fim];
            if(comparisonFinal == NSOrderedAscending){
                return YES;
            }
        }
    }
    return NO;
}

-(void)pagseguroButtonTouched{
    if ([self.delegate respondsToSelector:@selector(handleOpenPayment:)]) {
        [self.delegate performSelector:@selector(handleOpenPayment:) withObject:products];
    }
}

-(void)loadObjectsFromDataStore{
    NSFetchRequest *request = [Ingresso fetchRequest];
    NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    self.tickets = [NSMutableArray arrayWithArray:[Ingresso objectsWithFetchRequest:request]];
    
    
    for (Ingresso *i in [self.tickets copy]) {
        if (i.transacao) {
            [self.tickets removeObject:i];
        }
    }
    numberOfTickets = self.tickets.count;
    
    float sum = 0;

    
    for (Ingresso *i in self.tickets) {
        for (Conteudo *c in self.conteudos) {
            if ([c.conteudoId isEqualToNumber:i.conteudoId]) {
                for (TiposDeIngressos *t in c.tiposDeIngressos) {
                    if ([t.tiposDeIngressosId isEqualToNumber:i.tipoDeIngressoId]) {
                        sum += [t.valor floatValue];
                    }
                }
            }
        }
    }
    self.subtotalValue = [NSNumber numberWithFloat:(float)sum];
    self.serviceChargeValue = [NSNumber numberWithFloat:((float)SERVICE_CHARGE * [self.subtotalValue floatValue])];
    self.totalValue = [NSNumber numberWithFloat:[self.subtotalValue floatValue] + [self.serviceChargeValue floatValue]];
    
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    //[MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

-(void)handleTicketsPayment:(NSString *)transactionId{
    for (Ingresso *i in self.tickets) {
        i.transacao = transactionId;
        [objectManager putObject:i usingBlock:^(RKObjectLoader *loader) {
            loader.delegate = self;
            loader.resourcePath = [NSString stringWithFormat:@"/ingressos/%@", i.ingressoId];
        }];
    }
    
}

#pragma mark paypal



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        
        if ([self.delegate respondsToSelector:@selector(handleOpenTAU)]) {
            [self.delegate handleOpenTAU];
        }
    }

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (indexPath.section == 0) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

-(void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        for (UIView *view in [self.tableView cellForRowAtIndexPath:indexPath].subviews) {
            [view setAlpha:0.0];
        }
        [self.tableView cellForRowAtIndexPath:indexPath].backgroundColor =[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    }
}

-(void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        for (UIView *view in [self.tableView cellForRowAtIndexPath:indexPath].subviews) {
            [view setAlpha:1.0];
        }
        [self.tableView cellForRowAtIndexPath:indexPath].backgroundColor =[UIColor clearColor];
    }
}

- (void)tableView:(UITableView *)tableView  commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {    
    // If row is deleted, remove it from the list.
    if (indexPath.section == 0) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            for (UIView *view in [self.tableView cellForRowAtIndexPath:indexPath].subviews) {
                [view setAlpha:1.0];
            }
            [self.tableView cellForRowAtIndexPath:indexPath].backgroundColor =[UIColor clearColor];
            numberOfTickets--;
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:YES];
            [self performSelector:@selector(deleteObject:) withObject:indexPath afterDelay:0.5];
            
        }
    }
}

-(void)deleteObject:(NSIndexPath *)indexPath{
    [objectManager deleteObject:[self.tickets objectAtIndex:indexPath.row] usingBlock:^(RKObjectLoader *loader) {
        loader.delegate = self;
        loader.resourcePath = [NSString stringWithFormat:@"/ingressos/%@", [[self.tickets objectAtIndex:indexPath.row] ingressoId]];
    }];
    [self.tickets removeObjectAtIndex:indexPath.row];
    [self.tableView endUpdates];
}

#pragma mark RKRequestDelegate methods

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastUpdatedAt"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    [self loadObjectsFromDataStore];
    [HUD hideUIBlockingIndicator];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
}
 
- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    
    if ([request isGET]) {
        // Handling GET /foo.xml
        if ([response isOK]) {
             NSLog(@"Retrieved XML: %@", [response bodyAsString]);
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json
        if ([response isJSON]) {
            
        }
        
    } else if ([request isDELETE]) {
        
        if ([response isNotFound]) {
            NSLog(@"Not Found");
        }
        else{
            [objectManager loadObjectsAtResourcePath:@"/ingressos" delegate:self];
        }
    }
}

@end
