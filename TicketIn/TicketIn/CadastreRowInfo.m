//
//  CadastreRowInfo.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 30/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CadastreRowInfo.h"

@implementation CadastreRowInfo

@synthesize title;
@synthesize validValue;

+(NSMutableArray *)listOfItens{
    NSMutableArray *toReturn = [[NSMutableArray alloc] init];
    NSArray *array = [NSArray arrayWithObjects:@"Nome", @"Endereço", @"CEP", @"Cidade", @"Estado", @"Telef. Residencial", @"CPF", @"E-mail", @"Data Nasc.", @"Senha", @"Cofirmar Senha" , nil];
    for (int i = 0; i < 11; i++) {
        CadastreRowInfo *info = [[CadastreRowInfo alloc] init];
        info.title = [array objectAtIndex:i];
        info.validValue = [NSNumber numberWithBool:TRUE];
        info.textOfTextfield = @"";
        [toReturn addObject:info];
    }
    return toReturn;
}

@end

