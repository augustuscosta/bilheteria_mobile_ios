//
//  Empresa.m
//  TicketIn
//
//  Created by Nichollas Fonseca on 07/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Empresa.h"


@implementation Empresa

@dynamic cnpj;
@dynamic createAt;
@dynamic email;
@dynamic empresaId;
@dynamic identificar;
@dynamic nome;
@dynamic telefone;
@dynamic updatedAt;

@end
